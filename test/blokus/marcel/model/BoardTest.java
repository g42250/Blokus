package blokus.marcel.model;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Test for class Board.
 */
public class BoardTest {

    /**
     * Test of the constructor. No exception expected.
     */
    @Test
    public void testConstructeur() {
        Board board = new Board(20, 20);
    }

    /**
     * Test of the constructor. width = 0, exception expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructeurWidth0() {
        Board board = new Board(0, 20);
    }

    /**
     * Test of the constructor. height = 0, exception expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructeurHeight0() {
        Board board = new Board(20, 0);
    }

    /**
     * Test of getPieceAt method, of class Board. Position out of board, left.
     * Expected exception.
     */
    @Test
    public void testGetPieceAtOutLeft() {
        Board board = new Board(20, 20);
        assertNull(board.getPieceAt(new Position(-1, 0)));
    }

    /**
     * Test of getPieceAt method, of class Board. Position out of board, right.
     * Expected exception.
     */
    @Test
    public void testGetPieceAtOutRight() {
        Board board = new Board(20, 20);
        assertNull(board.getPieceAt(new Position(0, 20)));
    }

    /**
     * Test of getPieceAt method, of class Board. Position out of board, up.
     * Expected exception.
     */
    @Test
    public void testGetPieceAtOutUp() {
        Board board = new Board(20, 20);
        assertNull(board.getPieceAt(new Position(0, -1)));
    }

    /**
     * Test of getPieceAt method, of class Board. Position out of board, Bottom.
     * Expected exception.
     */
    @Test
    public void testGetPieceAtOutBot() {
        Board board = new Board(20, 20);
        assertNull(board.getPieceAt(new Position(0, 20)));
    }

    /**
     * Test of PlacePiece method, of class Board. Check if all cell is added.
     */
    @Test
    public void testPlacePieceCheckAllCell() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        listPosition.add(new Position(0, 1));
        listPosition.add(new Position(0, 2));
        Piece piece = new Piece(listPosition, Color.BLUE);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(0, 17), true);
        assertEquals(piece, board.getPieceAt(new Position(0, 19)));
        assertEquals(piece, board.getPieceAt(new Position(0, 18)));
        assertEquals(piece, board.getPieceAt(new Position(0, 17)));
    }

    /**
     * Test of PlacePiece method, of class Board. First turn of blue. One cell
     * must be in position 0, 19.
     */
    @Test
    public void testPlacePieceFirstTurnBlue() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.BLUE);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(0, 19), true);
        assertEquals(piece, board.getPieceAt(new Position(0, 19)));
    }

    /**
     * Test of PlacePiece method, of class Board. First turn of yellow. One cell
     * must be in position 0, 0.
     */
    @Test
    public void testPlacePieceFirstTurnYellow() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.YELLOW);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(0, 0), true);
        assertEquals(piece, board.getPieceAt(new Position(0, 0)));
    }

    /**
     * Test of PlacePiece method, of class Board. First turn of red. One cell
     * must be in position 19, 0.
     */
    @Test
    public void testPlacePieceFirstTurnRed() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.RED);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(19, 0), true);
        assertEquals(piece, board.getPieceAt(new Position(19, 0)));
    }

    /**
     * Test of PlacePiece method, of class Board. First turn of green. One cell
     * must be in position 19, 19.
     */
    @Test
    public void testPlacePieceFirstTurnGreen() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.GREEN);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(19, 19), true);
        assertEquals(piece, board.getPieceAt(new Position(19, 19)));
    }

    /**
     * Test of PlacePiece method, of class Board. First turn of blue. One cell
     * must be in position 0, 19. Greater piece.
     */
    @Test
    public void testPlacePieceFirstTurnGreaterPiece() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        listPosition.add(new Position(0, 1));
        listPosition.add(new Position(0, 2));
        Piece piece = new Piece(listPosition, Color.BLUE);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(0, 17), true);
        assertEquals(piece, board.getPieceAt(new Position(0, 17)));
    }

    /**
     * Test of PlacePiece method, of class Board. First turn of blue. One cell
     * must be in position 0, 19. Place in middle. Expected null.
     */
    @Test
    public void testPlacePieceFirstMiddleNull() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.BLUE);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(10, 10), true);
        assertNull(board.getPieceAt(new Position(10, 10)));
    }

    /**
     * Test of PlacePiece method, of class Board. First turn of blue. One cell
     * must be in position 0, 19. Place in other corner. Expected null.
     */
    @Test
    public void testPlacePieceFirstOtherCorner() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.BLUE);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(0, 0), true);
        assertNull(board.getPieceAt(new Position(0, 0)));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece not relied by a corner,
     * expected null.
     */
    @Test
    public void testPlacePieceNotRelied() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.YELLOW);
        Board board = new Board(20, 20);
        board.placePiece(piece, new Position(10, 10), false);
        assertNull(board.getPieceAt(new Position(10, 10)));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by the upper
     * right corner.
     */
    @Test
    public void testPlacePieceReliedUpperRight() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece1 = new Piece(listPosition, Color.BLUE);
        Piece piece2 = new Piece(listPosition, Color.BLUE);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(0, 19), true);
        board.placePiece(piece2, new Position(1, 18), false);
        assertEquals(piece2, board.getPieceAt(new Position(1, 18)));
    }

    /**
     * Test of PlacePiece method, of class Board. Piecerelied by the bottom
     * right corner.
     */
    @Test
    public void testPlacePieceReliedBottomRight() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece1 = new Piece(listPosition, Color.YELLOW);
        Piece piece2 = new Piece(listPosition, Color.YELLOW);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(0, 0), true);
        board.placePiece(piece2, new Position(1, 1), false);
        assertEquals(piece2, board.getPieceAt(new Position(1, 1)));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by the bottom
     * left corner.
     */
    @Test
    public void testPlacePieceReliedBottomLeft() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece1 = new Piece(listPosition, Color.RED);
        Piece piece2 = new Piece(listPosition, Color.RED);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(19, 0), true);
        board.placePiece(piece2, new Position(18, 1), false);
        assertEquals(piece2, board.getPieceAt(new Position(18, 1)));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by the upper left
     * corner.
     */
    @Test
    public void testPlacePieceReliedUpperLeft() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece1 = new Piece(listPosition, Color.GREEN);
        Piece piece2 = new Piece(listPosition, Color.GREEN);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(19, 19), true);
        board.placePiece(piece2, new Position(18, 18), false);
        assertEquals(piece2, board.getPieceAt(new Position(18, 18)));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by a corner but
     * not the same color, expected null.
     */
    @Test
    public void testPlacePieceReliedNotSameColor() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece pieceG = new Piece(listPosition, Color.GREEN);
        Piece pieceB = new Piece(listPosition, Color.BLUE);
        Board board = new Board(20, 20);
        board.placePiece(pieceG, new Position(19, 19), true);
        board.placePiece(pieceB, new Position(18, 18), false);
        assertNull(board.getPieceAt(new Position(18, 18)));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by a corner but
     * crush other piece, expected false.
     */
    @Test
    public void testCanAddPieceCrushOtherPiece() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece1 = new Piece(listPosition, Color.GREEN);
        Piece piece2 = new Piece(listPosition, Color.GREEN);
        Piece piece3 = new Piece(listPosition, Color.GREEN);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(19, 19), true);
        board.placePiece(piece2, new Position(18, 18), false);
        assertFalse(board.canAddPiece(piece3, new Position(18, 18), false));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by a corner but
     * touch upper contour, expected false.
     */
    @Test
    public void testCanAddPieceTouchUpperContour() {
        List<Position> listPosition1 = new ArrayList<>();
        listPosition1.add(new Position(0, 0));
        List<Position> listPosition2 = new ArrayList<>();
        listPosition2.add(new Position(0, 0));
        listPosition2.add(new Position(1, 0));
        Piece piece1 = new Piece(listPosition1, Color.GREEN);
        Piece piece2 = new Piece(listPosition2, Color.GREEN);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(19, 19), true);
        assertFalse(board.canAddPiece(piece2, new Position(18, 18), false));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by a corner but
     * touch left contour, expected false.
     */
    @Test
    public void testCanAddPieceTouchLeftContour() {
        List<Position> listPosition1 = new ArrayList<>();
        listPosition1.add(new Position(0, 0));
        List<Position> listPosition2 = new ArrayList<>();
        listPosition2.add(new Position(0, 0));
        listPosition2.add(new Position(0, 1));
        Piece piece1 = new Piece(listPosition1, Color.GREEN);
        Piece piece2 = new Piece(listPosition2, Color.GREEN);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(19, 19), true);
        assertFalse(board.canAddPiece(piece2, new Position(18, 18), false));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by a corner but
     * touch bottom contour, expected false.
     */
    @Test
    public void testCanAddPieceTouchBottomContour() {
        List<Position> listPosition1 = new ArrayList<>();
        listPosition1.add(new Position(0, 0));
        List<Position> listPosition2 = new ArrayList<>();
        listPosition2.add(new Position(0, 0));
        listPosition2.add(new Position(1, 0));
        Piece piece1 = new Piece(listPosition1, Color.YELLOW);
        Piece piece2 = new Piece(listPosition2, Color.YELLOW);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(0, 0), true);
        assertFalse(board.canAddPiece(piece2, new Position(0, 1), false));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece relied by a corner but
     * touch right contour, expected false.
     */
    @Test
    public void testCanAddPieceTouchRightContour() {
        List<Position> listPosition1 = new ArrayList<>();
        listPosition1.add(new Position(0, 0));
        List<Position> listPosition2 = new ArrayList<>();
        listPosition2.add(new Position(0, 0));
        listPosition2.add(new Position(0, 1));
        Piece piece1 = new Piece(listPosition1, Color.YELLOW);
        Piece piece2 = new Piece(listPosition2, Color.YELLOW);
        Board board = new Board(20, 20);
        board.placePiece(piece1, new Position(0, 0), true);
        assertFalse(board.canAddPiece(piece2, new Position(1, 0), false));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece already placed, expected
     * false.
     */
    @Test
    public void testCanAddPiecePlaced() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.YELLOW);
        piece.setPlaced(true);
        Board board = new Board(20, 20);
        assertFalse(board.canAddPiece(piece, new Position(0, 0), true));
    }

    /**
     * Test of PlacePiece method, of class Board. Position is null, expected
     * false.
     */
    @Test
    public void testCanAddPositionNull() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.YELLOW);
        piece.setPlaced(true);
        Board board = new Board(20, 20);
        assertFalse(board.canAddPiece(piece, null, true));
    }

    /**
     * Test of PlacePiece method, of class Board. Piece is null, expected false.
     */
    @Test
    public void testCanAddPieceNull() {
        Board board = new Board(20, 20);
        assertFalse(board.canAddPiece(null, new Position(0, 0), true));
    }

    /**
     * Test of getPieceAt method, of class Board. Null expected
     */
    @Test
    public void testGetPieceNull() {
        Board board = new Board(20, 20);
        assertNull(board.getPieceAt(new Position(10, 10)));
    }

    /**
     * Test of getNbPieceTaken method, of class Board. 0 expected
     */
    @Test
    public void testGetNbPosTaken0() {
        Board board = new Board(20, 20);
        assertEquals(0, board.getNbPosTaken());
    }

    /**
     * Test of getNbPieceTaken method, of class Board. 3 expected
     */
    @Test
    public void testGetNbPosTaken3() {
        List<Position> listPosition = new ArrayList<>();
        Board board = new Board(20, 20);
        listPosition.add(new Position(0, 0));
        listPosition.add(new Position(0, 1));
        listPosition.add(new Position(0, 2));
        Piece piece = new Piece(listPosition, Color.BLUE);
        board.placePiece(piece, new Position(0, 17), true);
        assertEquals(3, board.getNbPosTaken());
    }
}
