package blokus.marcel.model.ia;

import blokus.marcel.model.Position;
import org.junit.Test;

/**
 * Test for class PossibleMove
 */
public class PossibleMoveTest {

    /**
     * Test of constructor, of class PossibleMove. No exception expected.
     */
    @Test
    public void testConstructor() {
        PossibleMove move = new PossibleMove(0, new Position(0, 0), 0, true);
    }

    /**
     * Test of constructor, of class PossibleMove. Position null, exception
     * expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorPosNull() {
        PossibleMove move = new PossibleMove(0, null, 0, true);
    }
}
