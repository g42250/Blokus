package blokus.marcel.model;

import blokus.marcel.model.io.ReaderFile;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for class Stock.
 */
public class StockTest {

    /**
     * Test of the constructor. No exception expected.
     */
    @Test
    public void testConstructor() {
        ReaderFile rf = new ReaderFile();
        Stock stock = rf.createStock().get(0);
        List<Piece> listPiece = new ArrayList<>();
        for (int i = 0; i < 21; i++) {
            listPiece.add(stock.getPieceAt(i));
        }
        Stock testStock = new Stock(listPiece);
    }

    /**
     * Test of the constructor, list null. Exception expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorListNull() {
        List<Piece> listPiece = null;
        Stock testStock = new Stock(listPiece);
    }

    /**
     * Test of the constructor, size of the list not 21. Exception expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorListSizeNot21() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.BLUE);
        List<Piece> listPiece = new ArrayList<>();
        listPiece.add(piece);
        Stock testStock = new Stock(listPiece);
    }

    /**
     * Test of the constructor, color not the same. Exception expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorColorNotSame() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.BLUE);
        List<Piece> listPiece = new ArrayList<>();
        listPiece.add(piece);
        for (int i = 0; i < 20; i++) {
            listPiece.add(piece);
        }
        listPiece.add(new Piece(listPosition, Color.GREEN));
        Stock testStock = new Stock(listPiece);
    }

    /**
     * Test of getPieceAt method, of class Stock. i smaller than 0. Expected
     * exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetPieceAtSmallerThan0() {
        Stock stock = new ReaderFile().createStock().get(0);
        stock.getPieceAt(-1);
    }

    /**
     * Test of getPieceAt method, of class Stock. i greater than 20. Expected
     * exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetPieceAtGreaterThan20() {
        Stock stock = new ReaderFile().createStock().get(0);
        stock.getPieceAt(21);
    }

    /**
     * Test of getNbPiece method, of class Stock.
     */
    @Test
    public void testGetNbPiece() {
        Stock stock = new ReaderFile().createStock().get(0);
        assertEquals(21, stock.getNbPiece());
    }

    /**
     * Test of getNbPieceNotPlaced method, of class Stock.
     */
    @Test
    public void testGetNbPieceNotPlaced() {
        Stock stock = new ReaderFile().createStock().get(0);
        assertEquals(21, stock.getNbPieceNotPlaced());
    }

    /**
     * Test of getNbPieceNotPlaced method, of class Stock. one piece placed.
     */
    @Test
    public void testGetNbPieceNotPlacedExpected20() {
        Stock stock = new ReaderFile().createStock().get(0);
        stock.getPieceAt(5).setPlaced(true);
        assertEquals(20, stock.getNbPieceNotPlaced());
    }
}
