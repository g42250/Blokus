package blokus.marcel.model;

import blokus.marcel.model.io.ReaderFile;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for class PlayerManager.
 */
public class PlayerManagerTest {

    /**
     * Test of constructor of class PlayerManager. Normal Case, no exception
     * exptected.
     */
    @Test
    public void testConstructor() {
        PlayerManager playerManager = new PlayerManager(initPlayers());
    }

    /**
     * Test of constructor of class PlayerManager. List of player of size 3.
     * Expected exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorSize3() {
        List<Player> players = initPlayers();
        players.remove(0);
        PlayerManager playerManager = new PlayerManager(players);
    }

    /**
     * Test of constructor of class PlayerManager. Players with same color.
     * Exception expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorSameColor() {
        List<Player> players = new ArrayList<>();
        List<Player> playersTmp = initPlayers();
        for (Player player : initPlayers()) {
            players.add(playersTmp.get(0));
        }
        PlayerManager playerManager = new PlayerManager(players);
    }

    /**
     * Test of constructor of class PlayerManager. List of player is null.
     * Expected exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNull() {
        PlayerManager playerManager = new PlayerManager(null);
    }

    /**
     * Test of get method, of class PlayerManager. Number smaller than 0,
     * expected Exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGet_intSmaller0() {
        PlayerManager playerManager = new PlayerManager(initPlayers());
        playerManager.get(-1);
    }

    /**
     * Test of get method, of class PlayerManager. Number greater than 3,
     * expected Exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGet_intGreater3() {
        PlayerManager playerManager = new PlayerManager(initPlayers());
        playerManager.get(4);
    }

    /**
     * Test of toNextPlayer method, of class PlayerManager.
     */
    @Test
    public void testToNextPlayer() {
        PlayerManager playerManager = new PlayerManager(initPlayers());
        playerManager.toNextPlayer();
        assertEquals(playerManager.get(1), playerManager.getCurrentPlayer());
    }

    /**
     * Test of toNextPlayer method, of class PlayerManager. Next player can't
     * play. Expected the next next player.
     */
    @Test
    public void testToNextPlayerWithPlayerPass() {
        PlayerManager playerManager = new PlayerManager(initPlayers());
        playerManager.get(1).setCanPlay(false);
        playerManager.toNextPlayer();
        assertEquals(playerManager.get(2), playerManager.getCurrentPlayer());
    }

    /**
     * Test of getNbPlayerCanPlay method, of class PlayerManager. Expected 4.
     */
    @Test
    public void testGetNbPlayerCanPlay4() {
        PlayerManager playerManager = new PlayerManager(initPlayers());
        assertEquals(4, playerManager.getNbPlayerCanPlay());
    }

    /**
     * Test of getNbPlayerCanPlay method, of class PlayerManager. Expected 3.
     */
    @Test
    public void testGetNbPlayerCanPlay3() {
        PlayerManager playerManager = new PlayerManager(initPlayers());
        playerManager.get(2).setCanPlay(false);
        assertEquals(3, playerManager.getNbPlayerCanPlay());
    }

    /**
     * Test of getWinner method, of class PlayerManager. All player win.
     */
    @Test
    public void testGetWinnerAllWinner() {
        List<Player> players = initPlayers();
        PlayerManager playerManager = new PlayerManager(players);
        assertEquals(players, playerManager.getWinner());
    }

    /**
     * Test of getWinner method, of class PlayerManager. One player win.
     */
    @Test
    public void testGetWinnerOneWinner() {
        List<Player> players = initPlayers();
        List<Player> winnerExpected = new ArrayList<>();
        winnerExpected.add(players.get(0));
        PlayerManager playerManager = new PlayerManager(players);
        playerManager.get(0).getStock().getPieceAt(0).setPlaced(true);
        assertEquals(winnerExpected, playerManager.getWinner());
    }

    /**
     * @return a list of 4 players with differents colors.
     */
    private List<Player> initPlayers() {
        List<Player> listPlayer = new ArrayList<>();
        ReaderFile rf = new ReaderFile();
        List<Stock> listStock = rf.createStock();
        for (Stock stock : listStock) {
            listPlayer.add(new Player(stock));
        }
        return listPlayer;
    }
}
