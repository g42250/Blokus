package blokus.marcel.model;

import blokus.marcel.model.io.ReaderFile;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for class Player.
 */
public class PlayerTest {

    /**
     * Test of the constructor, no exception expected.
     */
    @Test
    public void testConstructor() {
        ReaderFile rf = new ReaderFile();
        Stock stock = rf.createStock().get(0);
        Player player = new Player(stock);
    }

    /**
     * Test of the constructor, stock null. Exception expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorStockNull() {
        Stock stock = null;
        Player player = new Player(stock);
    }

    /**
     * Test of getTotalScore method, of class Player. No piece placed.
     */
    @Test
    public void testGetScore() {
        Player player = initPlayer();
        assertEquals(-88, player.getScore());
    }

    /**
     * Test of getTotalScore method, of class Player. All piece Placed
     */
    @Test
    public void testGetScoreAllPiecePlaced() {
        Player player = initPlayer();
        for (int i = 0; i < 21; i++) {
            player.getStock().getPieceAt(i).setPlaced(true);
        }
        assertEquals(183, player.getScore());
    }

    /**
     * Test of getTotalScore method, of class Player. All piece placed, last is
     * the piece at index 0.
     */
    @Test
    public void testGetScoreAllPiecePlacedLastSize1() {
        Player player = initPlayer();
        for (int i = 0; i < 21; i++) {
            player.getStock().getPieceAt(i).setPlaced(true);
        }
        player.setLastPiece(player.getStock().getPieceAt(0));
        assertEquals(188, player.getScore());
    }

    /**
     * Test of getMalus method, of class Player. All piece placed, expected 0.
     */
    @Test
    public void testGetMalus0() {
        Player player = initPlayer();
        for (int i = 0; i < 21; i++) {
            player.getStock().getPieceAt(i).setPlaced(true);
        }
        assertEquals(0, player.getMalus());
    }

    /**
     * Test of getMalus method, of class Player. Any piece placed, expected 88.
     */
    @Test
    public void testGetMalus88() {
        Player player = initPlayer();
        assertEquals(88, player.getMalus());
    }

    /**
     * Test of setLastPiece method, of class Player. Piece is null, exception
     * expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testSetLastPieceNullException() {
        Player player = initPlayer();
        player.setLastPiece(null);
    }

    /**
     * Test of isFirstTurn method, of class Player. Any piece placed, expected
     * true.
     */
    @Test
    public void testIsFirstTurnTrue() {
        Player player = initPlayer();
        assertTrue(player.isFirstTurn());
    }

    /**
     * Test of isFirstTurn method, of class Player. One piece placed, expected
     * False.
     */
    @Test
    public void testIsFirstTurnFalse() {
        Player player = initPlayer();
        player.getStock().getPieceAt(0).setPlaced(true);
        assertFalse(player.isFirstTurn());
    }

    /**
     * @return A initialized player.
     */
    private Player initPlayer() {
        ReaderFile rf = new ReaderFile();
        Stock stock = rf.createStock().get(0);
        return new Player(stock);
    }
}
