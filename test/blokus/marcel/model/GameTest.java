package blokus.marcel.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for class Game.
 */
public class GameTest {

    /**
     * Test of constructor, no exception expected.
     */
    @Test
    public void testConstructor() {
        Game game = new Game(20, 20);
    }

    /**
     * Test of play method, of class Game. Check if the currentPlayer change to
     * null after the execution.
     */
    @Test
    public void testPlayPieceSelectedToNull() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.selectPiece(0, 0);
        game.play(game.getPieceSelected(), new Position(0, 19));
        assertNull(game.getPieceSelected());
    }

    /**
     * Test of play method, of class Game. Check if the currentPlayer change to
     * null after the execution, no ia in the game.
     */
    @Test
    public void testPlayChangeCurrentPlayer() {
        Game game = new Game(20, 20);
        game.beginGame();
        for (Player player : game.getPlayers()) {
            player.setIsIA(false);
        }
        game.selectPiece(0, 0);
        game.play(game.getPieceSelected(), new Position(0, 19));
        assertEquals(
                game.getPlayers().get(1), game.getPlayers().getCurrentPlayer());
    }

    /**
     * Test of play method, of class Game. Bad play, the currentPlayer must not
     * be changed.
     */
    @Test
    public void testPlayBadPlayDontChangeCurrentPlayer() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.selectPiece(0, 0);
        game.play(game.getPieceSelected(), new Position(1, 19));
        assertEquals(
                game.getPlayers().get(0), game.getPlayers().getCurrentPlayer());
    }

    /**
     * Test of play method, of class Game. Bad play, the piece selected must not
     * be changed.
     */
    @Test
    public void testPlayBadPlayDontChangePieceSelected() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.selectPiece(0, 0);
        game.play(game.getPieceSelected(), new Position(1, 19));
        assertEquals(game.getPlayers().getCurrentPlayer().getStock().
                getPieceAt(0), game.getPieceSelected());
    }

    /**
     * Test of play method, of class Game. False expected
     */
    @Test
    public void testPlayFalse() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.selectPiece(0, 0);
        assertFalse(game.play(game.getPieceSelected(), new Position(1, 19)));
    }

    /**
     * Test of play method, of class Game. True expected
     */
    @Test
    public void testPlayTrue() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.selectPiece(0, 0);
        assertTrue(game.play(game.getPieceSelected(), new Position(0, 19)));
    }

    /**
     * Test of selectPiece method, of class Game. Bad color, null expected
     */
    @Test
    public void testSelectPieceBadColor() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.selectPiece(1, 0);
        assertNull(game.getPieceSelected());
    }

    /**
     * Test of selectPiece method, of class Game. Piece placed, null expected
     */
    @Test
    public void testSelectPiecePlaced() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.getPlayers().getCurrentPlayer().getStock().getPieceAt(0).
                setPlaced(true);
        game.selectPiece(0, 0);
        assertNull(game.getPieceSelected());
    }

    /**
     * Test of selectPiece method, of class Game. NbPlayer out of range, null
     * expected
     */
    @Test
    public void testSelectnbPlayerOutOfRange() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.getPlayers().getCurrentPlayer().getStock().getPieceAt(0).
                setPlaced(true);
        game.selectPiece(4, 0);
        assertNull(game.getPieceSelected());
    }

    /**
     * Test of selectPiece method, of class Game. NbPiece out of range, null
     * expected
     */
    @Test
    public void testSelectnbPieceOutOfRange() {
        Game game = new Game(20, 20);
        game.beginGame();
        game.getPlayers().getCurrentPlayer().getStock().getPieceAt(0).
                setPlaced(true);
        game.selectPiece(0, 21);
        assertNull(game.getPieceSelected());
    }

    /**
     * Test of getPossibleMoves method, of class Game. Only test the size of the
     * list.
     */
    @Test
    public void testGetPossibleMoves() {
        Game game = new Game(20, 20);
        game.beginGame();
        assertEquals(116, game.getPossibleMoves().size());
    }
}
