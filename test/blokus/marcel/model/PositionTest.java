package blokus.marcel.model;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for class Position.
 */
public class PositionTest {

    /**
     * Test of the constructor, no exception expected.
     */
    @Test
    public void testConstructor() {
        Position pos = new Position(0, 0);
    }

    /**
     * Test of movePos method, of class Position.
     */
    @Test
    public void testMovePos_int_int() {
        Position pos = new Position(1, 3);
        Position expected = new Position(-4, 5);
        Position nPos = pos.movePos(-5, 2);
        assertEquals(expected, nPos);
    }

    /**
     * Test of movePos method, of class Position.
     */
    @Test
    public void testMovePos_Position() {
        Position pos = new Position(-2, 3);
        Position expected = new Position(3, -5);
        Position nPos = pos.movePos(new Position(5, -8));
        assertEquals(expected, nPos);
    }

    /**
     * Test of movePos method, of class Position. Pos null, expected exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMovePos_PositionNull() {
        Position pos = new Position(-2, 3);
        Position nPos = pos.movePos(null);
    }

    /**
     * Test of compareTo method, of class Position. Positions equals, expected
     * 0.
     */
    @Test
    public void testCompareTo0() {
        Position pos1 = new Position(5, 5);
        Position pos2 = new Position(5, 5);
        assertEquals(0, pos1.compareTo(pos2));
    }

    /**
     * Test of compareTo method, of class Position. Smaller x and y, expected
     * -1.
     */
    @Test
    public void testCompareToSmallerXAndY() {
        Position pos1 = new Position(4, 4);
        Position pos2 = new Position(5, 5);
        assertEquals(-1, pos1.compareTo(pos2));
    }

    /**
     * Test of compareTo method, of class Position. Smaller x equals y, expected
     * -1.
     */
    @Test
    public void testCompareToSmallerXEqualsY() {
        Position pos1 = new Position(4, 5);
        Position pos2 = new Position(5, 5);
        assertEquals(-1, pos1.compareTo(pos2));
    }

    /**
     * Test of compareTo method, of class Position. Smaller x greater y,
     * expected 1.
     */
    @Test
    public void testCompareToSmallerXGreaterY() {
        Position pos1 = new Position(4, 6);
        Position pos2 = new Position(5, 5);
        assertEquals(1, pos1.compareTo(pos2));
    }

    /**
     * Test of compareTo method, of class Position. Greater x, greater y,
     * expected 1.
     */
    @Test
    public void testCompareToGreaterXGreaterY() {
        Position pos1 = new Position(6, 6);
        Position pos2 = new Position(5, 5);
        assertEquals(1, pos1.compareTo(pos2));
    }

    /**
     * Test of compareTo method, of class Position. Greater x, equals y,
     * expected 1.
     */
    @Test
    public void testCompareToGreaterXEqualsY() {
        Position pos1 = new Position(6, 5);
        Position pos2 = new Position(5, 5);
        assertEquals(1, pos1.compareTo(pos2));
    }

    /**
     * Test of compareTo method, of class Position. Greater x, smaller y,
     * expected -1.
     */
    @Test
    public void testCompareToGreaterXSmallerY() {
        Position pos1 = new Position(6, 4);
        Position pos2 = new Position(5, 5);
        assertEquals(-1, pos1.compareTo(pos2));
    }
}
