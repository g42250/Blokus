package blokus.marcel.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test for class Piece.
 */
public class PieceTest {

    /**
     * Test of the constructor. No exception expected.
     */
    @Test
    public void testConstructor() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.BLUE);
    }

    /**
     * Test of the constructor, listPositon null. expected exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorListNull() {
        List<Position> listPosition = null;
        Piece piece = new Piece(listPosition, Color.BLUE);
    }

    /**
     * Test of the constructor, color null. expected exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorColorNull() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, null);
    }

    /**
     * Test of the constructor, listPositon empty. expected exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorListEmpty() {
        List<Position> listPosition = new ArrayList<>();
        Piece piece = new Piece(listPosition, Color.BLUE);
    }

    /**
     * Test of getScore method, of class Piece. Size of 1, expected 1.
     */
    @Test
    public void testGetScoreSize1() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        Piece piece = new Piece(listPosition, Color.BLUE);
        assertEquals(1, piece.getScore());
    }

    /**
     * Test of getScore method, of class Piece. Size of 2, expected 1.
     */
    @Test
    public void testGetScoreSize2() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        listPosition.add(new Position(0, 1));
        Piece piece = new Piece(listPosition, Color.BLUE);
        assertEquals(1, piece.getScore());
    }

    /**
     * Test of getScore method, of class Piece. Size of 3, expected 2.
     */
    @Test
    public void testGetScoreSize3() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        listPosition.add(new Position(0, 1));
        listPosition.add(new Position(0, 3));
        Piece piece = new Piece(listPosition, Color.BLUE);
        assertEquals(2, piece.getScore());
    }

    /**
     * Test of getScore method, of class Piece. Size of 4, expected 5.
     */
    @Test
    public void testGetScoreSize4() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 0));
        listPosition.add(new Position(0, 1));
        listPosition.add(new Position(0, 3));
        listPosition.add(new Position(0, 4));
        Piece piece = new Piece(listPosition, Color.BLUE);
        assertEquals(5, piece.getScore());
    }

    /**
     * Test of getScore method, of class Piece. Size of 5, expected 12.
     */
    @Test
    public void testGetScoreSize5() {
        assertEquals(12, initPiece().getScore());
    }

    /**
     * Test of getNbCell method, of class Piece.
     */
    @Test
    public void testGetNbCell() {
        assertEquals(5, initPiece().getNbCell());
    }

    /**
     * Test of posInBoard method, of class Piece. normal case, equals expected.
     */
    @Test
    public void testPosInBoard() {
        Piece piece = initPiece();
        List<Position> resultPos = new ArrayList<>();
        resultPos.add(new Position(3, 1));
        resultPos.add(new Position(3, 3));
        resultPos.add(new Position(4, 1));
        resultPos.add(new Position(4, 2));
        resultPos.add(new Position(4, 3));
        assertEquals(resultPos, piece.posInBoard(new Position(3, 1)));
    }

    /**
     * Test of posInBoard method, of class Piece. Position null, exception
     * expected.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPosInBoardNullPosException() {
        Piece piece = initPiece();
        piece.posInBoard(null);
    }

    /**
     * Test of Rotate method, of class Piece. Rotate by 0. Object must be not
     * modified.
     */
    @Test
    public void testRotateEquals0() {
        Piece piece = initPiece();
        Piece pieceResult = initPiece();
        piece.rotate(0);
        assertEquals(pieceResult, piece);
    }

    /**
     * Test of Rotate method, of class Piece. Rotate by 1. The piece must be
     * rotated by 90° in the clock sens. The order of the positions must be
     * correct.
     */
    @Test
    public void testRotateEquals1() {
        Piece piece = initPiece();
        piece.rotate(1);
        List<Position> positionsExpected = new ArrayList<>();
        positionsExpected.add(new Position(0, 0));
        positionsExpected.add(new Position(2, 0));
        positionsExpected.add(new Position(0, 1));
        positionsExpected.add(new Position(1, 1));
        positionsExpected.add(new Position(2, 1));
        assertEquals(positionsExpected, piece.getListPosition());
    }

    /**
     * Test of Rotate method, of class Piece. Rotate by 3. The piece must be
     * rotated by 90° in the clock sens. The order of the positions must be
     * correct.
     */
    @Test
    public void testRotateEquals3() {
        Piece piece = initPiece();
        piece.rotate(3);
        List<Position> positionsExpected = new ArrayList<>();
        positionsExpected.add(new Position(1, 2));
        positionsExpected.add(new Position(2, 2));
        positionsExpected.add(new Position(3, 2));
        positionsExpected.add(new Position(1, 3));
        positionsExpected.add(new Position(3, 3));
        assertEquals(positionsExpected, piece.getListPosition());
    }

    /**
     * Test of Rotate method, of class Piece. Rotate by 0. Expected false.
     */
    @Test
    public void testRotateBoolean0() {
        assertFalse(initPiece().rotate(0));
    }

    /**
     * Test of Rotate method, of class Piece. Piece place. Expected false.
     */
    @Test
    public void testRotateBooleanPlaced() {
        Piece piece = initPiece();
        piece.setPlaced(true);
        assertFalse(piece.rotate(1));
    }

    /**
     * Test of Rotate method, of class Piece. Piece placed. Object must be not
     * modified.
     */
    @Test
    public void testRotateEqualsPlaced() {
        Piece piece = initPiece();
        Piece pieceEcpected = initPiece();
        piece.setPlaced(true);
        pieceEcpected.setPlaced(true);
        piece.rotate(1);
        assertEquals(pieceEcpected, piece);
    }

    /**
     * Test of Rotate method, of class Piece. Rotate by 1. Expected true.
     */
    @Test
    public void testRotateBoolean1() {
        assertTrue(initPiece().rotate(1));
    }

    /**
     * Test of mirror method, of class Piece. The listPositions must be
     * correctly updated and be in the correct order.
     */
    @Test
    public void testMirrorEquals() {
        Piece piece = initPiece();
        List<Position> positionsExpected = new ArrayList<>();
        positionsExpected.add(new Position(2, 1));
        positionsExpected.add(new Position(3, 1));
        positionsExpected.add(new Position(2, 2));
        positionsExpected.add(new Position(2, 3));
        positionsExpected.add(new Position(3, 3));
        piece.mirror();
        assertEquals(positionsExpected, piece.getListPosition());
    }

    /**
     * Test of mirror method, of class Piece. Piece placed. The object must not
     * be modified.
     */
    @Test
    public void testMirrorPlacedEquals() {
        Piece piece = initPiece();
        Piece pieceExpected = initPiece();
        piece.setPlaced(true);
        pieceExpected.setPlaced(true);
        piece.mirror();
        assertEquals(pieceExpected, piece);
    }

    /**
     * Test of mirror method, of class Piece. Piece placed. False expected.
     */
    @Test
    public void testMirrorPlacedBoolean() {
        Piece piece = initPiece();
        Piece pieceExpected = initPiece();
        piece.setPlaced(true);
        pieceExpected.setPlaced(true);
        assertFalse(piece.mirror());
    }

    /**
     * Test of mirror method, of class Piece. True expected
     */
    @Test
    public void testMirrorBoolean() {
        assertTrue(initPiece().mirror());
    }

    /**
     * @return A piece initialized. It's the last piece of the stock of the blue
     * player.
     */
    private Piece initPiece() {
        List<Position> listPosition = new ArrayList<>();
        listPosition.add(new Position(0, 1));
        listPosition.add(new Position(0, 3));
        listPosition.add(new Position(1, 1));
        listPosition.add(new Position(1, 2));
        listPosition.add(new Position(1, 3));
        return new Piece(listPosition, Color.BLUE);
    }
}
