package blokus.marcel.model;

/**
 * Class who represents a player. Each player has a stock and a lastPiece. The
 * lastPiece is the lastPiece that the player has placed. It's used for
 * calculate the score of the player. It also have 3 booleans: hasPlayThisTurn
 * who determine if a player has played the current turn. canPlay who determine
 * a player can play. It used to choice to stop to play. isIa who determine is
 * the player is a ia.
 */
public class Player {

    private Stock stock;
    private Piece lastPiece;
    private boolean hasPlayThisTurn;
    private boolean canPlay;
    private boolean isIA;

    /**
     * Create a player. isIa is initialized to true.
     *
     * @param stock The stock of the player.
     * @throws IllegalArgumentException If the stock is null.
     */
    protected Player(Stock stock) {
        if (stock == null) {
            throw new IllegalArgumentException("stock must be not null");
        }
        this.stock = stock;
        hasPlayThisTurn = false;
        canPlay = true;
        isIA = true;
    }

    /**
     * Get the score of the player. It's calculate like this: addition of the
     * score of each piece placed + bonus points - x points by piece not placed.
     * x is the number of cell of a piece. The bonus point is equals to 15 if
     * each piece is placed and equals to 20 if each piece is placed with the
     * piece who have only one cell placed in last.
     *
     * @return The score of the player.
     */
    public int getScore() {
        int score = 0, nbPiecePut = 0;
        for (int i = 0; i < stock.getNbPiece(); i++) {
            if (stock.getPieceAt(i).isPlaced()) {
                score += stock.getPieceAt(i).getScore();
                nbPiecePut++;
            }
        }
        return score + countBonusPoint(nbPiecePut) - getMalus();
    }

    /**
     * @return The malus of the player. It equals of the addition of the number
     * of cell of each piece not placed.
     */
    public int getMalus() {
        int malus = 0;
        for (int i = 0; i < stock.getNbPiece(); i++) {
            if (!stock.getPieceAt(i).isPlaced()) {
                malus += stock.getPieceAt(i).getNbCell();
            }
        }
        return malus;
    }

    /**
     * Calculate the bonus point of the player. it return 15 if each piece is
     * placed or 20 if each piece are placed with the piece who have only one
     * cell place in last or 0 if all piece are not placed.
     *
     * @param nbPiecePut The number of piece placed.
     * @return The bonus point of the player.
     */
    private int countBonusPoint(int nbPiecePut) {
        int bonus = 0;
        if (nbPiecePut == 21) {
            bonus = 15;
            if (lastPiece == stock.getPieceAt(0)) {
                bonus = 20;
            }
        }
        return bonus;
    }

    /**
     * Return the stock of the player.
     *
     * @return The stock of the player;
     */
    public Stock getStock() {
        return this.stock;
    }

    /**
     * Return the color of the player.
     *
     * @return the color of the player.
     */
    public Color getColor() {
        //we have verirfied that all piece have the same color.
        return stock.getPieceAt(0).getColor();
    }

    /**
     * Set the last piece placed by the player.
     *
     * @param piece The piece who is the last to be placed by the player.
     * @throws IllegalArgumentException if the piece is null.
     */
    protected void setLastPiece(Piece piece) {
        if (piece == null) {
            throw new IllegalArgumentException("piece must be not null");
        }
        this.lastPiece = piece;
    }

    /**
     * @return true if it's the first turn of the player.
     */
    public boolean isFirstTurn() {
        return stock.getNbPieceNotPlaced() == 21;
    }

    /**
     * @return true if the player has played this turn.
     */
    public boolean hasPlayThisTurn() {
        return hasPlayThisTurn;
    }

    /**
     * Set hasPlayThisTurn with the new value.
     *
     * @param hasPlayThisTurn The new value.
     */
    protected void setHasPlayThisTurn(boolean hasPlayThisTurn) {
        this.hasPlayThisTurn = hasPlayThisTurn;
    }

    /**
     * @return canPlay.
     */
    public boolean canPlay() {
        return canPlay;
    }

    /**
     * Set canPlay
     *
     * @param canPlay The new value.
     */
    protected void setCanPlay(boolean canPlay) {
        this.canPlay = canPlay;
    }

    /**
     * @return isIa.
     */
    public boolean isIA() {
        return isIA;
    }

    /**
     * Set isIa.
     *
     * @param isIA The new velue.
     */
    protected void setIsIA(boolean isIA) {
        this.isIA = isIA;
    }

    /**
     * Create a string who represents the player. It will be represented by the
     * first char of the color of the player.
     *
     * @return A string who represents the player.
     */
    @Override
    public String toString() {
        return getColor().toString().toLowerCase();
    }
}
