package blokus.marcel.model;

import blokus.marcel.model.ia.PlayIa;
import blokus.marcel.model.ia.PossibleMove;
import blokus.marcel.model.io.ReaderFile;
import blokus.marcel.mvc.Observer;
import blokus.marcel.mvc.Subject;
import java.util.ArrayList;
import java.util.List;

/**
 * Class who represents a game. A game contains a board and a list of 4 players
 * Each player have a different color. observers is the list of observers
 * attached to this subject. playerManager who contains a list of player and
 * manage them. stop is a boolean. If it's false, the game can't be played
 * anymore. pieceSelected is the piece selected by the current player. width and
 * height are the width and the height of the board.
 */
public class Game implements Subject {

    private Board board;
    private final List<Observer> observers;
    private PlayerManager playerManager;
    private boolean stop;
    private Piece pieceSelected;
    private final int width;
    private final int height;

    /**
     * Create a game. The width and the height are tested in the class board,
     * they're must be strictly positive. Initialize the payerManager, stop to
     * true, the list of observers and the width and height.
     *
     * @param width The width of the board
     * @param height the height of the board
     */
    public Game(int width, int height) {
        this.observers = new ArrayList<>();
        this.stop = true;
        this.width = width;
        this.height = height;
    }

    /**
     * Try to add a piece on the board. Check the method canAddPiece of the
     * class board to know the condition for add a piece. If the piece is added,
     * return true. Else return false. Pass the turn if the piece is placed.
     * Notify the observers.
     *
     * @param piece The piece we want to add.
     * @param pos The position where we want to add the piece
     * @return true if the piece is added, else return false.
     */
    public boolean play(Piece piece, Position pos) {
        boolean isAdded = !stop && piece != null
                && !playerManager.get(piece.getColor()).hasPlayThisTurn();
        isAdded = isAdded && addPiece(piece, pos);
        if (isAdded) {
            playerManager.getCurrentPlayer().setHasPlayThisTurn(true);
            pieceSelected = null;
            checkStop();
            toNextPlayer();
            notifyAllObservers();
        }
        return isAdded;
    }

    /**
     * Pass the turn of the player.
     */
    private void toNextPlayer() {
        playerManager.toNextPlayer();
        if (playerManager.getCurrentPlayer().isIA()) {
            iaPlay();
        }
    }

    /**
     * Add a piece at the given position.
     *
     * @param piece The piece.
     * @param pos The position.
     * @return true if the piece is added, else return false.
     */
    private boolean addPiece(Piece piece, Position pos) {
        boolean isAdded = board.placePiece(piece, pos,
                playerManager.get(piece.getColor()).isFirstTurn());
        if (isAdded) {
            piece.setPlaced(true);
            playerManager.get(piece.getColor()).setLastPiece(piece);
            checkStop();
        }
        return isAdded;
    }

    /**
     * @return the playerManager
     */
    public PlayerManager getPlayers() {
        return playerManager;
    }

    /**
     * Check if at least one player can play.
     */
    private void checkStop() {
        stop = playerManager.getNbPlayerCanPlay() == 0;
    }

    /**
     * Return a boolean who determine if the game can continue.
     *
     * @return true if the game can continue, else return false.
     */
    public boolean getStop() {
        return stop;
    }

    /**
     * Set stop. Notify the observers.
     *
     * @param stop The new value of stop.
     */
    public void setStop(boolean stop) {
        this.stop = stop;
        notifyAllObservers();
    }

    /**
     * Add an observer at the list of observers.
     *
     * @param obs The observer we want to add.
     */
    @Override
    public void addObserver(Observer obs) {
        observers.add(obs);
    }

    /**
     * Remove an observer at the list of observers. If the observer is not in
     * the list, do nothing.
     *
     * @param obs The observer we want to remove.
     */
    @Override
    public void removeObserver(Observer obs) {
        observers.remove(obs);
    }

    /**
     * Launch the update method of all observer attached to this subject.
     */
    private void notifyAllObservers() {
        for (Observer obs : observers) {
            obs.update();
        }
    }

    /**
     * @return the board
     */
    public Board getBoard() {
        return board;
    }

    /**
     * Rotate the piece in position i in the stock of the player with the given
     * color nb times. Notify all observers if the piece is rotated.
     *
     * @param color The color of the player.
     * @param i The index of the piece in the stock.
     * @param nb The number of time to rotate the piece.
     * @return true if the piece is rotated, else return false.
     */
    public boolean rotate(Color color, int i, int nb) {
        boolean canRotate = i < 21 && i >= 0 && nb > 0;
        canRotate &= playerManager.get(color).getStock().getPieceAt(i).
                rotate(nb);
        if (canRotate && !playerManager.getCurrentPlayer().isIA()) {
            notifyAllObservers();
        }
        return canRotate;
    }

    /**
     * Rotate once time the selected piece. Notify all observers.
     */
    public void rotateSelectedPiece() {
        if (pieceSelected != null) {
            pieceSelected.rotate(1);
            notifyAllObservers();
        }
    }

    /**
     * set the selected piece. The player must not have played this turn and the
     * piece not be placed. Notify the observers.
     *
     * @param nbPlayer The player number.
     * @param nbPiece The index of the piece in the stock.
     */
    public void selectPiece(int nbPlayer, int nbPiece) {
        Piece piece = null;
        if (nbPlayer >= 0 && nbPlayer < 4 && nbPiece >= 0 && nbPiece < 21) {
            piece = getPlayers().get(nbPlayer).getStock().getPieceAt(nbPiece);
        }
        if (piece != null && !getPlayers().get(nbPlayer).hasPlayThisTurn()
                && !piece.isPlaced() && 
                piece.getColor() == playerManager.getCurrentColor()) {
            pieceSelected = getPlayers().get(nbPlayer).getStock().
                    getPieceAt(nbPiece);
            notifyAllObservers();
        }
    }

    /**
     * @return the selected piece.
     */
    public Piece getPieceSelected() {
        return pieceSelected;
    }

    /**
     * Initialise a game and notify the observers.
     */
    public void beginGame() {
        this.stop = false;
        pieceSelected = null;   //not neccessary, but just to be sure...
        initGame();
        if (playerManager.getCurrentPlayer().isIA()) {
            iaPlay();
        }
        notifyAllObservers();
    }

    /**
     * Initialize a game of blokus. The game have 4 players with 21 pieces and a
     * board of 20x20.
     *
     * @return A game of blokus.
     */
    private void initGame() {
        List<Player> listPlayer = new ArrayList<>();
        ReaderFile rf = new ReaderFile();
        List<Stock> listStock = rf.createStock();
        for (Stock stock : listStock) {
            listPlayer.add(new Player(stock));
        }
        this.playerManager = new PlayerManager(listPlayer);
        this.board = new Board(width, height);
    }

    /**
     * Mirror the selected piece and notify all observers. If the piece selected
     * is null, do nothing.
     */
    public void mirrorSelectedPiece() {
        if (pieceSelected != null) {
            if (pieceSelected.mirror()) {
                notifyAllObservers();
            }
        }
    }

    /**
     * Make the current player pass. He can't play anymore during the rest of
     * the game. Notify observersssss.
     */
    public void currentPlayerPass() {
        if (!stop) {
            playerManager.getCurrentPlayer().setCanPlay(false);
            toNextPlayer();
            checkStop();
            notifyAllObservers();
        }
    }

    /**
     * Mirror the piece at the index i in the stock of the current player. If
     * the piece can't be mirror do nothing. Notify all observers.
     *
     * @param i the index of the piece in the stock of the current player.
     */
    public void mirrorPieceAt(int i) {
        Piece piece = playerManager.getCurrentPlayer().getStock().getPieceAt(i);
        if (piece != null && !piece.isPlaced()) {
            piece.mirror();
            if (!playerManager.getCurrentPlayer().isIA()) {
                notifyAllObservers();
            }
        }

    }

    /**
     * Check if the current player is a ia. If it the case, it make it play.
     */
    private void iaPlay() {
        if (!stop && playerManager.getCurrentPlayer().isIA()) {
            new PlayIa(this).play();
        }
    }

    /**
     * @return a list of possible moves of the current player.
     */
    public List<PossibleMove> getPossibleMoves() {
        List<PossibleMove> possibleMoves = new ArrayList<>();
        Player player = playerManager.getCurrentPlayer();
        for (int y = 0; y < 20; y++) {
            for (int x = 0; x < 20; x++) {
                testPiece(new Position(x, y), player, possibleMoves);
            }
        }
        return possibleMoves;
    }

    /**
     * Test all piece of a player if they can be placed. Add the move in the
     * given list if the move can be played. Test also each rotate of the piece.
     *
     * @param pos The position.
     * @param player The player.
     * @param possibleMoves The list to update.
     */
    private void testPiece(Position pos, Player player,
            List<PossibleMove> possibleMoves) {
        for (int i = 0; i < player.getStock().getNbPiece(); i++) {
            for (int nbRotate = 0; nbRotate < 4; nbRotate++) {
                testPlay(pos, i, nbRotate, possibleMoves);
            }
        }
    }

    /**
     * Test a move and add it to the given list if the move can be played. Test
     * also the piece mirrored.
     *
     * @param pos The position.
     * @param nbPiece The index of the piece in the stock of the current player.
     * @param nbRotate the nb time to rotate the piece.
     * @param possibleMoves The list to update.
     */
    private void testPlay(Position pos, int nbPiece, int nbRotate, 
            List<PossibleMove> possibleMoves) {
        Player player = playerManager.getCurrentPlayer();
        Piece piece = new Piece(player.getStock().getPieceAt(nbPiece));
        piece.rotate(nbRotate);
        if (board.canAddPiece(piece, pos, 
                playerManager.getCurrentPlayer().isFirstTurn())) {
            possibleMoves.add(new PossibleMove(nbPiece, pos, nbRotate, false));
        }
        piece.mirror();
        if (board.canAddPiece(piece, pos, 
                playerManager.getCurrentPlayer().isFirstTurn())) {
            possibleMoves.add(new PossibleMove(nbPiece, pos, nbRotate, true));
        }
    }
}
