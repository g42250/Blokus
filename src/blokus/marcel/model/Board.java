package blokus.marcel.model;

import java.util.List;

/**
 * Class who represents a board. Generally, it's a board of 20x20. The point
 * (0,0) is on the upper left corner. posPiecePlaced is a list of position where
 * a piece is placed.
 */
public class Board {

    private Piece[][] board;
    private int width;
    private int height;

    /**
     * Create a board. Generally, the width and the height have the value 20.
     *
     * @param width The width of the board.
     * @param height The height of the board.
     * @throws IllegalArgumentException if the height or the width are not
     * strictly positive.
     */
    protected Board(int width, int height) {
        if (width < 1 || height < 1) {
            throw new IllegalArgumentException("width and height must be "
                 + "striclty positive. Width: " + width + " height: " + height);
        }
        this.board = new Piece[height][width];
        this.width = width;
        this.height = height;
    }

    /**
     * Return the piece at the given position. If there are no piece or if the
     * pos is not in the board, return null.
     *
     * @param pos The position we want to get the piece.
     * @return The piece at the given position.
     * @throws IllegalArgumentException if the position is null.
     */
    public Piece getPieceAt(Position pos) {
        if (pos == null) {
            throw new IllegalArgumentException("The position must not be null");
        }
        Piece piece = null;
        if (this.isPosInBoard(pos)) {
            piece = board[pos.getY()][pos.getX()];
        }
        return piece;
    }

    /**
     * Get the color of the piece at the giben position. If there are not piece
     * in the position, return null.
     *
     * @param pos The position.
     * @return The color at the given position.
     */
    private Color getColorAt(Position pos) {
        Color color;
        Piece piece = getPieceAt(pos);
        if (piece == null) {
            color = null;
        } else {
            color = piece.getColor();
        }
        return color;
    }

    /**
     * set a cell with a piece at the given position. If the position is out of
     * the board or if the piece is null, don't add the piece.
     *
     * @param piece The piece we want to add.
     * @param pos The position where we want to add the piece.
     * @return true if the piece is added, else return false.
     */
    private boolean setCase(Piece piece, Position pos) {
        boolean isAdded = false;
        if (piece != null && pos != null && this.isPosInBoard(pos)) {
            board[pos.getY()][pos.getX()] = piece;
            isAdded = true;
        }
        return isAdded;
    }

    /**
     * Check if the given piece can be placed at the given position without crus
     * an other piece. Also check, if this is not the first turn, if the given
     * piece is relied with an other piece with the same color.
     *
     * @param piece The piece we cant to check.
     * @param pos The position.
     * @param isFirstTurn true if it's the first turn.
     * @return true if the piece can be placed without crush an other piece and
     * if it relied with an other piece with the same color if this is not the
     * first turn.
     */
    private boolean isNotCrush(Piece piece, Position pos, boolean isFirstTurn) {
        List<Position> posPiece = piece.posInBoard(pos);
        boolean isNotTrample = true;
        boolean isRelied = isFirstTurn;
        int i = 0;
        while (isNotTrample && i < posPiece.size()) {
            isNotTrample = this.getPieceAt(posPiece.get(i)) == null && 
                    checkContour(piece.getColor(), posPiece.get(i));
            if (!isFirstTurn) {
                isRelied |= checkCornerRelied(piece, posPiece.get(i));
            }
            i++;
        }
        return isNotTrample && isRelied;
    }

    /**
     * Check if a cell don't touch an other cell who contains the given color.
     *
     * @param color The color.
     * @param pos The position of the cell.
     * @return true if the cell don't touch an other cell who contains the given
     * color.
     */
    private boolean checkContour(Color color, Position pos) {
        return getColorAt(new Position(pos.getX() - 1, pos.getY())) != color
               && getColorAt(new Position(pos.getX() + 1, pos.getY())) != color
               && getColorAt(new Position(pos.getX(), pos.getY() - 1)) != color
               && getColorAt(new Position(pos.getX(), pos.getY() + 1)) != color;
    }

    /**
     * Check if we can place a piece at the given position who will be relied
     * with an other piece of the same color by a corner.
     *
     * @param piece The piece.
     * @param pos The position.
     * @return true if the piece is relied by a corner.
     */
    private boolean checkCornerRelied(Piece piece, Position pos) {
        boolean isRelied = false;
        Piece piecePos;
        for (int x = pos.getX() - 1; x <= pos.getX() + 1; x = x + 2) {
            for (int y = pos.getY() - 1; y <= pos.getY() + 1; y = y + 2) {
                piecePos = this.getPieceAt(new Position(x, y));
                isRelied |= piecePos != null && piecePos.getColor()
                        == piece.getColor();
            }
        }
        return isRelied;
    }

    /**
     * Check if we can place a piece for the first turn at the given position.
     *
     * @param piece The piece.
     * @param pos The position.
     * @return true if we can place the first at the given position for the
     * first turn.
     */
    private boolean checkFirstTurn(Piece piece, Position pos) {
        return piece != null && piece.posInBoard(pos).
                contains(posFirstTurn(piece.getColor()));
    }

    /**
     * Check if a position is not null and in the board.
     *
     * @param pos The position we want to check.
     * @return true if the position is not null and in the board, else return
     * false.
     */
    public boolean isPosInBoard(Position pos) {
        return pos != null && pos.getX() >= 0 && pos.getX() < width
                && pos.getY() >= 0 && pos.getY() < height;
    }

    /**
     * Check if we can put a piece at a given position. We can added it if the
     * piece and the pos are not null, if the piece is not placed, if the
     * position is on the board, if the piece don't crush an other piece, if the
     * contour of the piece don't contains the same color of the piece. If it
     * the first turn, the piece must be at a determinate corner. If it's not
     * the firs turn of the player, the piece must be relied by a corner with an
     * other piece of the same color.
     *
     * @param piece The piece we cant to check if we can put on the board.
     * @param pos The position where we want to check if we can put a piece.
     * @param isFirstTurn true if it the first turn of the player.
     * @return true if we can put a piece, else return false.
     */
    public boolean canAddPiece(Piece piece, Position pos, boolean isFirstTurn) {
        boolean canAdd = piece != null && pos != null && !piece.isPlaced() &&
             this.isPosInBoard(pos) && this.isNotCrush(piece, pos, isFirstTurn);
        canAdd = canAdd
               && ((isFirstTurn && checkFirstTurn(piece, pos)) || !isFirstTurn);
        int i = 0;
        if (canAdd) {
            List<Position> posInBoard = piece.posInBoard(pos);
            while (canAdd && i < posInBoard.size()) {
                canAdd = this.isPosInBoard(posInBoard.get(i));
                i++;
            }
        }
        return canAdd;
    }

    /**
     * Place a piece at the given position. If we can't add the piece, do
     * nothing and return false.
     *
     * @param piece The piece we want to add.
     * @param pos The position where we want to add the piece. It's here that
     * the upper left corner of the piece will be.
     * @param isFirstTurn true if it the first turn of the player.
     * @return true if the piece is added, else return false.
     */
    protected boolean placePiece(Piece piece, Position pos, boolean isFirstTurn) {
        boolean canAdd = this.canAddPiece(piece, pos, isFirstTurn);
        if (canAdd) {
            for (Position posPiece : piece.posInBoard(pos)) {
                this.setCase(piece, posPiece);
            }
            piece.setPlaced(true);
        }
        return canAdd;
    }

    /**
     * @return the number of cell taked in the board.
     */
    public int getNbPosTaken() {
        int nb = 0;
        for (Piece[] line : board) {
            for (Piece piece : line) {
                if (piece != null) {
                    nb++;
                }
            }
        }
        return nb;
    }

    /**
     * Get the position where begin a player. The player must play a part of a
     * piece on this position in his first turn.
     *
     * @param color The color of the player.
     * @return The position where a player begin. If the color is not know,
     * return null.
     */
    public Position posFirstTurn(Color color) {
        Position pos;
        switch (color) {
            case BLUE:
                pos = new Position(0, height - 1);
                break;

            case YELLOW:
                pos = new Position(0, 0);
                break;

            case RED:
                pos = new Position(width - 1, 0);
                break;

            case GREEN:
                pos = new Position(width - 1, height - 1);
                break;

            default:
                pos = null;

        }
        return pos;
    }

    /**
     * @return the witdh of the board.
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return The height the board
     */
    public int getHeight() {
        return height;
    }

    /**
     * Create a string of the board. This string contains a "*" or a color of a
     * player for each cell of the board. If the cell don't contains a piece,
     * it's a "*". Else it's the first character of the color of the player who
     * have the piece on this cell.
     *
     * @return A string of the board.
     */
    @Override
    public String toString() {
        String str = "";
        for (Piece[] line : board) {
            for (Piece piece : line) {
                if (piece == null) {
                    str += "*";
                } else {
                    str += piece.getColor().getChar();
                }
            }
            str += System.getProperty("line.separator");
        }
        return str;
    }
}
