package blokus.marcel.model.io;

import blokus.marcel.model.Color;
import blokus.marcel.model.Piece;
import blokus.marcel.model.Position;
import blokus.marcel.model.Stock;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class who read a file for creating a list of piece.
 */
public class ReaderFile {

    private Scanner scan;
    final private String resourcesPath = "descriptorPiece";
    private final InputStream stream
            = ReaderFile.class.getResourceAsStream(resourcesPath);

    /**
     * Initialize the scanner.
     *
     * @throws NullPointerException if descriptorPiece is not found.
     */
    public ReaderFile() {
        if (stream == null) {
            throw new NullPointerException("descriptorPiece not found");
        }
        scan = new Scanner(stream);
    }

    /**
     * Read one line of the file if not at the end. If the file is at end,
     * return null.
     *
     * @return One line of the file.
     */
    private String readLine() {
        String line = null;
        if (!isEnd()) {
            line = scan.nextLine();
        }
        return line;
    }

    /**
     * Check if we are at the end of the file.
     *
     * @return true if the file is at the end.
     */
    private boolean isEnd() {
        return !scan.hasNext();
    }

    /**
     * Initialize a list of list of piece.
     *
     * @return a list of list of piece.
     */
    private List<List<Piece>> createListPiece() {
        List<List<Piece>> listPiece = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            listPiece.add(new ArrayList<>());
        }
        return listPiece;
    }

    /**
     * Create a stock based on a file. Each stock have a different color. This
     * method must be not called more than one time in a same instance.
     *
     * @return A list of stock.
     */
    public List<Stock> createStock() {
        char[][] arrayChar;
        int i;
        List<List<Piece>> listPiece = createListPiece();
        String line;
        while (!isEnd()) {
            i = 0;
            line = readLine();
            if (line != null && line.length() == 4) {
                arrayChar = createArrayChar(line);
                for (Color color : Color.values()) {
                    listPiece.get(i).add(
                            new Piece(createListPosition(arrayChar), color));
                    i++;
                }
            }
        }
        scan.close();
        return listToStock(listPiece);
    }

    /**
     * create an array of char of size 4x4. This array will represent a piece.
     * This method continue to read the file until it fall on a line who not
     * contains 4 char.
     *
     * @param line The line to begin
     * @return an array of char of size 4x4.
     */
    private char[][] createArrayChar(String line) {
        char[][] arrayChar = new char[4][4];
        int j = 0;
        while (line != null && line.length() == 4) {
            for (int i = 0; i < line.length(); i++) {
                arrayChar[j][i] = line.charAt(i);
            }
            j++;
            line = readLine();
        }
        return arrayChar;
    }

    /**
     * create 4 stock with the given list of 4 list of piece.
     *
     * @param listPiece The list of 4 list of piece.
     * @return a list of 4 stock
     * @throws IllegalArgumentException If listPiece is null or if the size of
     * listPiece is not 4.
     */
    private List<Stock> listToStock(List<List<Piece>> listPiece) {
        if (listPiece == null) {
            throw new IllegalArgumentException("listPiece must not be null");
        } else if (listPiece.size() != 4) {
            throw new IllegalArgumentException("The size of listPiece must be "
                    + "equals to 4. Size: " + listPiece.size());
        }
        List<Stock> listStock = new ArrayList<>();
        for (List<Piece> list : listPiece) {
            listStock.add(new Stock(list));
        }
        return listStock;
    }

    /**
     * Create a list of position based on a array of char. This list will
     * contains a position for each "x" in the array.
     *
     * @param arrayChar The array of char
     * @return A list of position.
     */
    private List<Position> createListPosition(char[][] arrayChar) {
        List<Position> listPosition = new ArrayList<>();
        int i = 0, j = 0;
        for (char[] lineChar : arrayChar) {
            for (char chara : lineChar) {
                if (chara == 'x') {
                    listPosition.add(new Position(i, j));
                }
                i++;
            }
            j++;
            i = 0;
        }
        return listPosition;
    }
}
