package blokus.marcel.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Class who represents a playerManager. It manage the players and the current
 * player.
 */
public class PlayerManager implements Iterable<Player> {

    private List<Player> players;
    private Player currentPlayer;
    private int index;

    /**
     * Create a playerManager. Ask a list of 4 players and make by default the
     * first player to no ia.
     *
     * @param players a list of 4 players.
     * @throws IllegalArgumentException if the list is null or if it not
     * contains 4 players or if there are at least 2 players with the same
     * color.
     */
    protected PlayerManager(List<Player> players) {
        if (players == null) {
            throw new IllegalArgumentException("listPlayers must not be null");
        }
        if (players.size() != 4) {
            throw new IllegalArgumentException("There must be 4 player. Actual"
                    + "number of player: " + players.size());
        }
        if (!allPlayerDifColor(players)) {
            throw new IllegalArgumentException("All player must have different"
                    + "Color");
        }
        this.players = players;
        this.index = 0;
        this.currentPlayer = players.get(0);
        players.get(0).setIsIA(false);
    }

    /**
     * Check if all player have an unique color.
     *
     * @param listPlayers The list of player we want to check.
     * @return true if each player have an unique color. Else, return false.
     */
    private boolean allPlayerDifColor(List<Player> listPlayers) {
        boolean playerDifColor = true;
        List<Color> listColor = new ArrayList<>();
        for (Player player : listPlayers) {
            if (listColor.contains(player.getColor())) {
                playerDifColor = false;
            }
            listColor.add(player.getColor());
        }
        return playerDifColor;
    }

    /**
     * @return a iterator.
     */
    @Override
    public Iterator<Player> iterator() {
        return players.iterator();
    }

    /**
     * Get the player at the index i.
     *
     * @param i The index.
     * @return The player at the index i.
     * @throws IllegalArgumentException if the index is out of range.
     */
    public Player get(int i) {
        if (i < 0 || i >= players.size()) {
            throw new IllegalArgumentException("i is out of range. Size of "
                    + "the list : " + players.size() + " i: " + i);
        }
        return players.get(i);
    }

    /**
     * @param color The color.
     * @return The player with the given color. If there are not player with
     * this color, return null.
     */
    public Player get(Color color) {
        for (Player player : this) {
            if (color == player.getColor()) {
                return player;
            }
        }
        return null;
    }

    /**
     * Pass the turn to the next player who can play.
     */
    protected void toNextPlayer() {
        index = (index += 1) % players.size();
        currentPlayer = players.get(index);
        checkStock();
        if (!currentPlayer.canPlay() && getNbPlayerCanPlay() > 0) {
            this.toNextPlayer();
        } else if (getNbPlayerCanPlay() != 0) {
            currentPlayer.setHasPlayThisTurn(false);
        }
    }

    /**
     * @return The current player.
     */
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * @return The color of the current player.
     */
    public Color getCurrentColor() {
        return currentPlayer.getColor();
    }

    /**
     * @return The number of player who can play.
     */
    public int getNbPlayerCanPlay() {
        int nb = 0;
        for (Player player : players) {
            if (player.canPlay()) {
                nb++;
            }
        }
        return nb;
    }

    /**
     * Check if the current player has not an empty stock and set the boolean
     * canPlay of the player.
     */
    private void checkStock() {
        currentPlayer.setCanPlay(currentPlayer.canPlay()
                && currentPlayer.getStock().getNbPieceNotPlaced() != 0);
    }

    /**
     * Get the winner the player who has the greater score.
     *
     * @return the player who is the winner.
     */
    public List<Player> getWinner() {
        int score = -99;
        List<Player> winners = new ArrayList<>();
        for (Player player : players) {
            if (player.getScore() > score) {
                winners.clear();
                score = player.getScore();
            }
            if (player.getScore() == score) {
                winners.add(player);
            }
        }
        return winners;
    }
}
