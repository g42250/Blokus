package blokus.marcel.model;

/**
 * class who represents a position. A position has a x and a y. This class is
 * immutable. Also, it implements comparable. Two instances of this class can
 * easilly be compared.
 */
public final class Position implements Comparable {

    private final int x;
    private final int y;

    /**
     * create a position.
     *
     * @param x The position in the axe of x.
     * @param y The position in the axe of y.
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * Create a string on the form (x,y).
     *
     * @return a string on the form (x,y).
     */
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    /**
     * Create a new position moved in x by dx and in y by dy.
     *
     * @param dx The distance in x we want to move.
     * @param dy The distance in y we want to move.
     * @return A new position moved.
     */
    protected Position movePos(int dx, int dy) {
        return new Position(x + dx, y + dy);
    }

    /**
     * Create a new Position, the original position is moved by the distance in
     * x and y represented by a given position.
     *
     * @param pos Representation of the distance to move in position.
     * @return a new Position moved.
     */
    protected Position movePos(Position pos) {
        if (pos == null) {
            throw new IllegalArgumentException("pos must be not null");
        }
        return movePos(pos.getX(), pos.getY());
    }

    /**
     * Compare this instance with an other of this class. Return a negative
     * number if this instance is less than the other object. 0 if equals and a
     * positif number if it greater. Position with a greater or lesser 'y' will
     * always be lesser or greater. X is compared only if y i equals on the 2
     * positions.
     *
     * @param o The other instance.
     * @return a negative number if this instance is lesser than the other
     * object. 0 if equals or a positif number if it greater.
     * @throws NullPointerException if o is null.
     * @throws ClassCastException if o is not a Position.
     */
    @Override
    public int compareTo(Object o) {
        if (o == null) {
            throw new NullPointerException("o must not be null");
        }
        if (o.getClass() != this.getClass()) {
            throw new ClassCastException("o must be from the class Position");
        }
        Position other = (Position) o;
        int result = this.getY() - other.getY();
        if (result == 0) {
            result = this.getX() - other.getX();
        }
        return result;
    }

    /**
     * Compare 2 Position. The positions are equals if they're from the same
     * class, have the same x and the same y.
     *
     * @param obj The object we want to compare with.
     * @return True if the position is equals, else return false.
     */
    @Override
    public boolean equals(Object obj) {
        boolean isEquals = false;
        if (obj != null && obj.getClass() == this.getClass()) {
            Position other = (Position) obj;
            isEquals = other.x == this.x && other.y == this.y;
        }
        return isEquals;
    }

    /**
     * Create an hashCode
     *
     * @return an hashCode.
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.x;
        hash = 97 * hash + this.y;
        return hash;
    }
}
