package blokus.marcel.model.ia;

import blokus.marcel.model.Game;

/**
 * Class who make an ia play.
 */
public class PlayIa {

    private final Game game;
    private Context context;

    /**
     * Create a PlayIa.
     *
     * @param game The game.
     * @throws IllegalArgumentException If game is null.
     */
    public PlayIa(Game game) {
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.game = game;
    }

    /**
     * Make the ia play if the player can play.
     */
    public void play() {
        context = new Context(new IaTurn(game));
        if (game.getPlayers().getCurrentPlayer().canPlay()) {
            context.play();
        }
    }
}
