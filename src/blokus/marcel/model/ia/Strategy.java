package blokus.marcel.model.ia;

/**
 * interface who represents a strategy. It make the ia play.
 */
public interface Strategy {

    /**
     * Method to make the ia play. It place a piece in the board for the current
     * player.
     */
    public void play();
}
