package blokus.marcel.model.ia;

import blokus.marcel.model.Position;

/**
 * Class who represents a possibleMove. It contain nbPiece, it's the index of
 * the piece in the stock of the current player. Position is the position of the
 * move. NbRotate is the number of time to rotate the piece. IsMirror determine
 * if the piece have to be mirrored. The class don't verify if the move can be
 * applied. You must check it before.
 */
public class PossibleMove {

    private int nbPiece;
    private Position position;
    private int nbRotate;
    private boolean isMirror;

    /**
     * Create a possibleMove.
     *
     * @param nbPiece The index of the piece in the stock of a player.
     * @param position The position of the move.
     * @param nbRotate The number of time to rotate the piece.
     * @param isMirror contains true if the piece have to be mirrored.
     * @throws IllegalArgumentException if position is null.
     */
    public PossibleMove(int nbPiece, Position position, int nbRotate,
            boolean isMirror) {
        if (position == null) {
            throw new IllegalArgumentException("position must not be null");
        }
        this.nbPiece = nbPiece;
        this.position = position;
        this.nbRotate = nbRotate;
        this.isMirror = isMirror;
    }

    /**
     * @return nbPiece.
     */
    public int getNbPiece() {
        return nbPiece;
    }

    /**
     * @return position.
     */
    public Position getPosition() {
        return position;
    }

    /**
     * @return nbRotate.
     */
    public int getNbRotate() {
        return nbRotate;
    }

    /**
     * @return isMirror
     */
    public boolean isMirror() {
        return isMirror;
    }
}
