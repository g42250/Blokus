package blokus.marcel.model.ia;

import blokus.marcel.model.Game;
import blokus.marcel.model.Player;
import blokus.marcel.model.Position;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class who represents a turn of a ia. It contains a game, the current player
 * and a list of possible moves.
 */
public class IaTurn implements Strategy {

    private final Game game;
    private final Player player;
    private final List<PossibleMove> possibleMoves;
    private final Random rand = new Random();

    /**
     * Create an IaTurn.
     *
     * @param game The game
     * @throws IllegalArgumentException If game is null.
     */
    protected IaTurn(Game game) {
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.game = game;
        this.player = game.getPlayers().getCurrentPlayer();
        possibleMoves = new ArrayList<>();
    }

    /**
     * Make the ia play. This create a list of move, get the moves who have the
     * better score and the nearest position to the middle of the board and
     * execute the move. If they are multiple move left, make a random.
     */
    @Override
    public void play() {
        int scoreActual = 0;
        List<PossibleMove> test = game.getPossibleMoves();
        for (PossibleMove possibleMove : test) {
            if (game.getPlayers().getCurrentPlayer().getStock().getPieceAt(
                    possibleMove.getNbPiece()).getNbCell() > scoreActual) {
                possibleMoves.clear();
                scoreActual = game.getPlayers().getCurrentPlayer().getStock().
                        getPieceAt(possibleMove.getNbPiece()).getNbCell();
            }
            if (game.getPlayers().getCurrentPlayer().getStock().getPieceAt(
                    possibleMove.getNbPiece()).getNbCell() == scoreActual) {
                possibleMoves.add(possibleMove);
            }
        }
        chooseAndExecuteMove();
    }

    /**
     * choose a move in the list of move. It gets the nearest move to the center
     * of the board, make a random to choice the move and execute it. If the
     * size of the list of move is null, make the player pass. He can't plat
     * anymore.
     */
    private void chooseAndExecuteMove() {
        if (possibleMoves.size() > 0) {
            List<PossibleMove> posMoves = new ArrayList<>();
            Position middle = new Position(10, 10);
            int scorePos = 99;
            for (PossibleMove possibleMove : possibleMoves) {
                if (comparePos(possibleMove.getPosition(), middle) < scorePos) {
                    scorePos = comparePos(possibleMove.getPosition(), middle);
                    posMoves.clear();
                }
                if (comparePos(possibleMove.getPosition(), middle) == scorePos) {
                    posMoves.add(possibleMove);
                }
            }
            placePiece(posMoves.get(rand.nextInt(posMoves.size())));
        } else {
            game.currentPlayerPass();
        }
    }

    /**
     * Place a piece determinate by a move.
     *
     * @param move The move.
     */
    private void placePiece(PossibleMove move) {
        game.rotate(player.getColor(), move.getNbPiece(), move.getNbRotate());
        if (move.isMirror()) {
            game.mirrorPieceAt(move.getNbPiece());
        }
        game.play(player.getStock().getPieceAt(move.getNbPiece()),
                move.getPosition());

    }

    /**
     * Compare 2 positions. It return the difference between the x and the y of
     * the positions. It's different than the method compareTo from the Position
     * class.
     *
     * @param pos1 The first position.
     * @param pos2 The second position.
     * @return a negatif number if pos1 is smaller than pos2, 0 if they are
     * equals or a positive number if pos1 is greater than pos2.
     */
    private int comparePos(Position pos1, Position pos2) {
        return Math.abs(pos1.getX() - pos2.getX() + pos1.getY() - pos2.getY());
    }
}
