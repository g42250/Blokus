package blokus.marcel.model.ia;

/**
 * Class who represents a context.
 */
public class Context {

    private final Strategy strategy;

    /**
     * Create a context
     *
     * @param strategy A strategy.
     * @throws IllegalArgumentException If strategy is null.
     */
    protected Context(Strategy strategy) {
        if (strategy == null) {
            throw new IllegalArgumentException("strategy must not be null");
        }
        this.strategy = strategy;
    }

    /**
     * make the ia play.
     */
    protected void play() {
        strategy.play();
    }
}
