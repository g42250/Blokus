package blokus.marcel.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class who represents a Piece. Each piece have a list of position, these
 * position represents the piece in an array of 4x4. The point 0,0 of this array
 * is on the upperLeft corner. When we put a piece, we give a position. This
 * position will determine the first position in listPosition in the board.
 * color is the color of the piece. Placed determine if the piece is placed or
 * no.
 */
public class Piece {

    private List<Position> listPosition;
    private Color color;
    private boolean placed;

    /**
     * Create a piece.
     *
     * @param listPosition the list of position of the piece, this represents
     * the piece in an array of 4x4.
     * @param color The color of the piece.
     * @throws IllegalArgumentException if the listPosition or the color si null
     * and if the listPostion is empty.
     */
    public Piece(List<Position> listPosition, Color color) {
        if (listPosition == null || color == null) {
            throw new IllegalArgumentException("color and listPosition must not"
                    + " be null. listPosition: " + listPosition
                    + " color: " + color);
        }
        if (listPosition.isEmpty()) {
            throw new IllegalArgumentException("listPosition must not be empty "
                    + "or null");
        }
        this.listPosition = listPosition;
        this.color = color;
    }

    /**
     * Copy a piece.
     *
     * @param piece The piece to copy.
     */
    public Piece(Piece piece) {
        this.listPosition = piece.listPosition.subList(0,
                piece.listPosition.size());
        this.color = piece.color;
        this.placed = piece.placed;
    }

    /**
     * @return The color of the piece.
     */
    public Color getColor() {
        return color;
    }

    /**
     * Create a copy of the list of position of the piece and return it.
     *
     * @return the list of position of the piece.
     */
    public List<Position> getListPosition() {
        return listPosition.subList(0, listPosition.size());
    }

    /**
     * Set the boolean placed who determine if the piece is placed on the board
     * or not.
     *
     * @param placed the new value of placed.
     */
    protected void setPlaced(boolean placed) {
        this.placed = placed;
    }

    /**
     * Return true if the piece is placed, else return false.
     *
     * @return True if the piece is placed, else return false.
     */
    public boolean isPlaced() {
        return this.placed;
    }

    /**
     * Get the score of the piece. The score is a value between 1 and 12
     * determined by the number of cell of the piece.
     *
     * @return The score of the piece.
     */
    public int getScore() {
        int score;
        switch (listPosition.size()) {
            case 1:
            case 2:
                score = 1;
                break;
            case 3:
                score = 2;
                break;
            case 4:
                score = 5;
                break;
            case 5:
            default:
                score = 12;
        }
        return score;
    }

    /**
     * Calculate the number of cell of the piece.
     *
     * @return The number of cell of the piece.
     */
    public int getNbCell() {
        return listPosition.size();
    }

    /**
     * Return each position of the piece compared at the given pos. The given
     * position will be the upperLeft (the more upper before the more left)
     * corner of the piece. The others position of the piece start at this
     * position.
     *
     * @param pos The position of the upperLeft corner of the piece in the
     * board.
     * @return A list of position of a piece where the given position is the
     * upper left corner of the piece.
     * @throws IllegalArgumentException If the position is null.
     */
    public List<Position> posInBoard(Position pos) {
        if (pos == null) {
            throw new IllegalArgumentException("pos must be not null.");
        }
        List<Position> posPiece = this.getListPosition();
        Position posInvert = new Position(posPiece.get(0).getX() * -1,
                posPiece.get(0).getY() * -1);
        List<Position> posInBoard = new ArrayList<>();
        for (Position posi : posPiece) {
            posInBoard.add(posi.movePos(posInvert).movePos(pos));
        }
        return posInBoard;
    }

    /**
     * Rotate the piece nb times at 90° in the clock direction. If the rotation
     * is a succes, sort the listPosition for having the upperleft corner in
     * index 0. A piece placed can't be rotated. Return a boolean who determine
     * if the piece is rotated or not. A piece can be rotated if it is not
     * placed and if nb is stricly positive.
     *
     * @param nb The nb times to rotate the piece.
     * @return true if the piece is rotated, else return false.
     */
    protected boolean rotate(int nb) {
        boolean canRotate = !isPlaced() && nb > 0;
        if (canRotate) {
            for (int i = 0; i < nb; i++) {
                rotate();
            }
            listPosition.sort(null);
        }
        return canRotate;

    }

    /**
     * Rotate the piece of 90° in the clock direction.
     */
    private void rotate() {
        List<Position> newListPosition = new ArrayList<>();
        for (Position pos : listPosition) {
            newListPosition.add(new Position(3 - pos.getY(), pos.getX()));
        }
        listPosition = newListPosition;
    }

    /**
     * Mirror the piece. The piece can be mirrored if it is not placed. Sort te
     * list position if the piece is mirrored and return true.
     *
     * @return true if the piece is mirrored, else return false.
     */
    protected boolean mirror() {
        if (!isPlaced()) {
            List<Position> newListPosition = new ArrayList<>();
            for (Position pos : listPosition) {
                newListPosition.add(new Position(3 - pos.getX(), pos.getY()));
            }
            listPosition = newListPosition;
            listPosition.sort(null);
        }
        return !isPlaced();
    }

    /**
     * Create a string of the piece represented on an array of 4x4. the string
     * will contains "*" for the empty cell and "x" for the cell where the piece
     * have a position on it.
     *
     * @return A string who represents the piece.
     */
    @Override
    public String toString() {
        String str = "";
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                if (listPosition.contains(new Position(i, j))) {
                    str += "x";
                } else {
                    str += "*";
                }
            }
            str += System.getProperty("line.separator");
        }
        return str;
    }

    /**
     * Compare 2 Piece. They are equals if they're from this class, they have
     * the same color, they're placed or not together and if they listPosition
     * are equals.
     *
     * @param obj The object we want to compare with.
     * @return true if obj is equals to the instance, elser return false.
     */
    @Override
    public boolean equals(Object obj) {
        boolean isEquals = false;
        if (obj != null && obj.getClass() == this.getClass()) {
            Piece other = (Piece) obj;
            isEquals = this.color == other.color
                    && this.isPlaced() == other.isPlaced()
                    && this.compListPosition(other);
        }
        return isEquals;
    }

    /**
     * Compare the list of position with an other piece.
     *
     * @param piece The other piece.
     * @return true if they are equals. Else return false.
     */
    private boolean compListPosition(Piece piece) {
        boolean isEquals = piece.getListPosition().size()
                == this.getListPosition().size();
        int i = 0;
        while (isEquals && i < piece.getListPosition().size()) {
            isEquals = piece.getListPosition().get(i).equals(
                    this.getListPosition().get(i));
            i++;
        }
        return isEquals;
    }

    /**
     * @return The hashCode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.listPosition);
        hash = 71 * hash + Objects.hashCode(this.color);
        hash = 71 * hash + (this.placed ? 1 : 0);
        return hash;
    }
}
