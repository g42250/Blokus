package blokus.marcel.model;

import java.util.List;

/**
 * Class who represents a stock. A stock have a list of 21 different piece of
 * the same color. The list of piece is gived by the class ReaderFile.
 */
public class Stock {

    private final List<Piece> listPiece;

    /**
     * Create a stock.
     *
     * @param listPiece The list of 21 piece.
     * @throws IllegalArgumentException If listPiece is null, have not 21 piece
     * or if each piece have not the same color.
     */
    public Stock(List<Piece> listPiece) {
        if (listPiece == null) {
            throw new IllegalArgumentException("listPiece must not be null");
        }
        if (listPiece.size() != 21) {
            throw new IllegalArgumentException("The size of listPiece must be "
                    + "equals to 21. Size of listPiece: " + listPiece.size());
        } else if (!allPieceSameColor(listPiece)) {
            throw new IllegalArgumentException("all Piece must be of the same "
                    + "color");
        }
        this.listPiece = listPiece;
    }

    /**
     * return the piece at the index i.
     *
     * @param i The index of the piece we want.
     * @return The piece at the index i.
     * @throws IllegalArgumentException If i is not between 0 and 20.
     */
    public Piece getPieceAt(int i) {
        if (i < 0 || i >= listPiece.size()) {
            throw new IllegalArgumentException("i must be strictly greater "
                    + "than -1 and smaller than the size of listPiece. "
                    + "i: " + i + " size of listPiece: " + listPiece.size());
        }
        return listPiece.get(i);
    }

    /**
     * return the number of pieces in the stock.
     *
     * @return the number of pieces in the stock.
     */
    public int getNbPiece() {
        return listPiece.size();
    }

    /**
     * Return the number of piece not placed in the stock.
     *
     * @return the number of piece not placed in the stock.
     */
    public int getNbPieceNotPlaced() {
        int NbPieceNotPlaced = getNbPiece();
        for (Piece piece : listPiece) {
            if (piece.isPlaced()) {
                NbPieceNotPlaced--;
            }
        }
        return NbPieceNotPlaced;
    }

    /**
     * Check if all piece have the same color.
     *
     * @param listPiece The list of piece we want to test.
     * @return true if all piece have the same color, else return false.
     */
    private boolean allPieceSameColor(List<Piece> listPiece) {
        Boolean allSameColor = true;
        int i = 1;
        while (allSameColor && i < listPiece.size()) {
            allSameColor = listPiece.get(i).getColor()
                    == listPiece.get(0).getColor();
            i++;
        }
        return allSameColor;
    }

    /**
     * Create a string who represents the stock. The string will contains all
     * piece not placed of the stock with his index.
     *
     * @return A string who represents the stock.
     */
    @Override
    public String toString() {
        String str = "";
        int i = 0;
        for (Piece piece : listPiece) {
            if (!piece.isPlaced()) {
                str += i;
                str += System.getProperty("line.separator");
                str += piece.toString();
                str += System.getProperty("line.separator");
            }
            i++;
        }
        return str;
    }
}
