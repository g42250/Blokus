package blokus.marcel.model;

/**
 * Enum who represents the color of the players.
 */
public enum Color {
    BLUE,
    YELLOW,
    RED,
    GREEN;

    /**
     * Put the first char of the color to lower case and return it.
     *
     * @return The first char of the color in lower case.
     */
    public char getChar() {
        return this.toString().toLowerCase().charAt(0);
    }
}
