package blokus.marcel.view;

import blokus.marcel.model.Game;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * Class who represents a InfoPlayerHBox. It extends HBox. This HBox contains 2
 * label. One for The Player number and one other for the score of the player.
 * nbPlayer contain the player's number. The spacing between each children is
 * 25px.
 */
public class InfoPlayerHBox extends HBox {

    private Label lbPlayer;
    private Label lbNbScore;
    private Label lbNbPieceRemain;
    private Label lbCanPlay;
    private final int nbPlayer;
    private final Game game;

    /**
     * Create and initialize a InfoPlayerHBox.
     *
     * @param width The width of the HBox.
     * @param height The height of the HBox.
     * @param nbPlayer The player's number.
     * @param game The game of the javadoc.
     */
    protected InfoPlayerHBox(double width, double height, int nbPlayer, Game game) {
        super(15);
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.nbPlayer = nbPlayer;
        this.game = game;
        init();
    }

    /**
     * Initialize The InfoPlayerHBox and create the 2 labels.
     */
    private void init() {
        lbPlayer = new Label("Player " + (nbPlayer + 1));
        Label lbScore = new Label("score:");
        lbScore.setText("score:");
        lbNbScore = new Label("");
        lbNbScore = new Label("");
        Label lbPieceRemain = new Label("piece remaining:");
        lbNbPieceRemain = new Label("");
        lbCanPlay = new Label("");
        this.getChildren().addAll(lbPlayer, lbScore, lbNbScore, lbPieceRemain,
                lbNbPieceRemain, lbCanPlay);
    }

    /**
     * set the string in the label lbCanPlay.
     *
     * @param canPlay If the player can play or not.
     */
    private void setLbCanPlay(boolean canPlay) {
        if (canPlay) {
            lbCanPlay.setText(String.valueOf("ok"));
        } else {
            lbCanPlay.setText(String.valueOf("pass"));
        }
    }

    /**
     * Set the nb piece remain in the label lbNBPieceRemain.
     *
     * @param nbPiece the nb piece remain.
     */
    private void setLbPiece(int nbPiece) {
        lbNbPieceRemain.setText(String.valueOf(nbPiece));
    }

    /**
     * set the background of the label lbPlayer in yellow. Used to show the
     * actual player.
     */
    private void setlbPlayerColor() {
        lbPlayer.setStyle("-fx-background-color: yellow;");
    }

    /**
     * Reset style of lbPlayer.
     */
    private void resetlbPlayerColor() {
        lbPlayer.setStyle("");
    }

    /**
     * Refresh the display of the layout. Set the score, the number of piece
     * left and put in yellow if it's the current player.
     */
    protected void refresh() {
        setLbScore();
        setLbPiece(game.getPlayers().get(nbPlayer).getStock().
                getNbPieceNotPlaced());
        setLbCanPlay(game.getPlayers().get(nbPlayer).canPlay());
        if (game.getPlayers().getCurrentPlayer().getColor()
                == game.getPlayers().get(nbPlayer).getColor()) {
            setlbPlayerColor();
        } else {
            resetlbPlayerColor();
        }
    }

    private void setLbScore() {
        int score = game.getPlayers().get(nbPlayer).getScore()
                + game.getPlayers().get(nbPlayer).getMalus();
        int malus = game.getPlayers().get(nbPlayer).getMalus();
        String str = String.valueOf(score);
        if (malus > 0) {
            str += " (-" + malus + ")";
        }
        lbNbScore.setText(str);
    }
}
