package blokus.marcel.view;

import javafx.scene.layout.VBox;
import blokus.marcel.model.Color;
import blokus.marcel.model.Game;

/**
 * Class who represents a StatPlayerVBox. It extends VBox. It contains the color
 * of the player, an InfoPlayerHBox and a StockPane. It will show all the stat
 * of a player.
 */
public class StatPlayerVBox extends VBox {

    private final double width;
    private final double height;
    private final Color color;
    private final InfoPlayerHBox infoPlayer;
    private final StockPane stockPane;

    /**
     * Create and initialize a StatPlayerVBox.
     * 
    * @param width the width of the VBox
     * @param height the height of the VBox.
     * @param nbPlayer The number of the player.
     * @param game The game of the application.
     */
    protected StatPlayerVBox(double width, double height, int nbPlayer,
            Game game) {
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = width;
        this.height = height;
        this.color = Color.values()[nbPlayer];
        infoPlayer = new InfoPlayerHBox(this.width, 25, nbPlayer, game);
        stockPane = new StockPane(this.width, this.height - 25, nbPlayer, game);
        init();
    }

    /**
     * Initialize a StatPlayerVBox.
     */
    private void init() {
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        this.setStyle("-fx-border-width: 2px;"
                + "-fx-border-color: #7f7f7f;");
        this.getChildren().addAll(infoPlayer, stockPane);
    }

    /**
     * Get the color of the player.
     *
     * @return The color of the player.
     */
    protected Color getColor() {
        return color;
    }

    /**
     * Get the InfoPlayerHBox.
     *
     * @return The InfoPlayerHBox.
     */
    protected InfoPlayerHBox getInfoPlayerHBox() {
        return infoPlayer;
    }

    /**
     * Get the stockPane.
     *
     * @return The stockPane.
     */
    protected StockPane getStockPane() {
        return stockPane;
    }

    /**
     * Refresh the layout. It's include the infoPlayer and the stockPane.
     */
    protected void refresh() {
        infoPlayer.refresh();
        stockPane.refresh();
    }
}
