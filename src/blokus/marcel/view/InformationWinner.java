package blokus.marcel.view;

import blokus.marcel.model.Game;
import blokus.marcel.model.Player;
import java.util.List;
import javafx.scene.control.Alert;

/**
 * Class who reprents an alert who inform on the winner.
 *
 * @author flo
 */
public class InformationWinner extends Alert {

    private Game game;

    /**
     * Create an InformationWinner.
     *
     * @param game The game.
     * @throws IllegalArgumentException if the game is null.
     */
    protected InformationWinner(Game game) {
        super(AlertType.INFORMATION);
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.game = game;
        init();
    }

    /**
     * initialize the alert. Set the title to Winner and remove the HeaderText.
     */
    private void init() {
        this.setTitle("Winner");
        this.setHeaderText(null);
    }

    /**
     * Refresh the display. If the game is stoped and there are at least one
     * piece on the board, show the alert with the winner of the game.
     */
    protected void refresh() {
        if (game.getStop() && game.getBoard().getNbPosTaken() > 0) {
            List<Player> winners = game.getPlayers().getWinner();
            if (winners.size() == 1) {
                this.setContentText("The winner is the player "
                        + winners.get(0));
            } else {
                this.setContentText("The winners are players "
                        + getStrWinners(winners));
            }
            this.show();
        }
    }

    /**
     * Return a string who show the winner(s).
     *
     * @param winners The list of the winners.
     * @return A string who show the winner(s).
     */
    private String getStrWinners(List<Player> winners) {
        String strWinner = "";
        for (int i = 0; i < winners.size(); i++) {
            strWinner += winners.get(i);
            if (i == winners.size() - 2) {
                strWinner += " and ";
            } else if (i == winners.size() - 1) {
                strWinner += ".";
            } else {
                strWinner += ", ";
            }
        }
        return strWinner;
    }
}
