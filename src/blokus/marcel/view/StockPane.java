package blokus.marcel.view;

import blokus.marcel.model.Color;
import blokus.marcel.model.Game;
import blokus.marcel.model.Piece;
import blokus.marcel.model.Position;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.layout.GridPane;

/**
 * Class who represents a StockPane. It extends GridPane. It contains a
 * nbPlayer, it's the number of the player, a listPiecePane, it's a list of 21
 * PiecePace who display the stock of the player. It contains a list of 21
 * PiecePane.
 */
public class StockPane extends GridPane {

    private final double width;
    private final double height;
    private final int nbPlayer;
    private final List<PiecePane> listPiecePane;
    private final Game game;

    /**
     * Create and initialize a StockPane.
     *
     * @param width The width of the StockPane.
     * @param height The height of the StockPane.
     * @param nbPlayer The numbe of the player.
     * @param game The game of the application.
     */
    protected StockPane(double width, double height, int nbPlayer, Game game) {
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = width;
        this.height = height;
        this.game = game;
        this.nbPlayer = nbPlayer;
        listPiecePane = new ArrayList<>();
        this.setVgap(1);
        this.setHgap(1);
        init();
    }

    /**
     * Initialize The StockPane. It create 21 PiecePane.
     */
    private void init() {
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        PiecePane piecePane;
        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 7; i++) {
                piecePane = new PiecePane(width / 7, nbPlayer, i + (7 * j), game);
                listPiecePane.add(piecePane);
                this.add(piecePane, i, j);
                piecePane.setDisable(true);
            }
        }
    }

    /**
     * Get The PiecePane at the index nb.
     *
     * @param nb The index of the PiecePane we want.
     * @return The PiecePane at the index nb.
     */
    private PiecePane getPiecePaneAt(int nb) {
        return listPiecePane.get(nb);
    }

    /**
     * Refresh the stock of a player. Color the cell of the gridPane for display
     * the stock left of the player.
     */
    private void setColorStock() {
        Color color = game.getPlayers().get(nbPlayer).getColor();
        Piece piece;
        for (int i = 0; i < 21; i++) {
            piece = game.getPlayers().get(color).getStock().getPieceAt(i);
            if (piece != null && !piece.isPlaced()) {
                for (Position pos : piece.getListPosition()) {
                    getPiecePaneAt(i).setColor(pos.getX(), pos.getY(), color);
                }
            }
        }
    }

    /**
     * Reset the stockPane.
     */
    private void reset() {
        for (int i = 0; i < 21; i++) {
            getPiecePaneAt(i).reset();
        }
    }

    /**
     * refresh the stockPane.
     */
    protected void refresh() {
        reset();
        setColorStock();
        if (game.getStop()) {
            for (GridPane piecePane : listPiecePane) {
                piecePane.setDisable(true);
            }
        } else {
            for (GridPane piecePane : listPiecePane) {
                piecePane.setDisable(false);
            }
        }
    }
}
