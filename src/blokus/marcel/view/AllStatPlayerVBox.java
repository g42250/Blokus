package blokus.marcel.view;

import blokus.marcel.model.Game;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.layout.VBox;

/**
 * Class who represents all stats of all players. It's extends VBox. It have a
 * width, a height and a list of statPlayer.
 */
public class AllStatPlayerVBox extends VBox {

    private final double width;
    private final double height;
    private final List<StatPlayerVBox> statPlayer;

    /**
     * Create and initialize a VBox AllStatPlayer. This VBox have 5 px spacing
     * between his children, a given width and a given height.
     *
     * @param width The width of the VBox.
     * @param height The height of the VBox.
     * @param game the game of the application.
     */
    protected AllStatPlayerVBox(double width, double height, Game game) {
        super(5);
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = width;
        this.height = height;
        statPlayer = new ArrayList<>();
        init(game);
    }

    /**
     * Initialize the VBox.
     *
     * @param game The game of the application.
     */
    private void init(Game game) {
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        for (int i = 0; i < 4; i++) {
            StatPlayerVBox vBoxPlayer = new StatPlayerVBox(width, height / 4 - 2, i, game);
            statPlayer.add(vBoxPlayer);
            this.getChildren().add(vBoxPlayer);
        }
    }

    /**
     * Refresh all the statPlayer. It include the infoPlayer and the stockPane.
     */
    protected void refresh() {
        for (StatPlayerVBox stat : statPlayer) {
            stat.refresh();
        }
    }
}
