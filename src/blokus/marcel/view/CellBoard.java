package blokus.marcel.view;

import blokus.marcel.model.Position;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Class who represents a cellBoard. It's the cell of the GridPane of the board.
 * It contains a positions who represents the position of the cell on the board
 * and a lbl who is set to X When a piece can't be placed.
 */
public class CellBoard extends VBox {

    private Position pos;
    private Label lblX;

    /**
     * Create a CellBoard.
     *
     * @param width The width.
     * @param height The height.
     * @param pos The position.
     * @throws IllegalArgumentException if pos is null.
     */
    protected CellBoard(double width, double height, Position pos) {
        if (pos == null) {
            throw new IllegalArgumentException("pos must not be null");
        }
        this.pos = pos;
        initLblX();
        this.setAlignment(Pos.CENTER);
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
    }

    /**
     * @return The pos.
     */
    protected Position getPos() {
        return pos;
    }

    /**
     * Initialize the lbl. Set the text to 'X', hide it, set the style and add
     * it to the cell.
     */
    private void initLblX() {
        this.lblX = new Label("X");
        this.lblX.setVisible(false);
        this.lblX.setTextFill(Color.web("gray"));
        this.lblX.setStyle("-fx-font: 20 arial;");
        this.getChildren().add(lblX);
    }

    /**
     * Show the lblX.
     */
    protected void showX() {
        this.lblX.setVisible(true);
    }

    /**
     * Hide the lblX.
     */
    protected void hideX() {
        this.lblX.setVisible(false);
    }
}
