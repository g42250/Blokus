package blokus.marcel.view;

import blokus.marcel.model.Game;
import blokus.marcel.model.Piece;
import blokus.marcel.model.Position;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Class who represents a PaneBoard. It extends GridPane. it Contains a list of
 * 400 VBox in an array of 20x20. These VBox will have the color of the piece
 * put on the position where is the VBox. IF there are not piece, it will be
 * black.
 */
public class PaneBoard extends GridPane {

    private final double height;
    private final double width;
    private final List<CellBoard> listVBox = new ArrayList<>();
    private final Game game;

    /**
     * Create and initialize a PaneBoard.
     *
     * @param size The size of the PaneBoard.
     * @param game The game of the application.
     */
    protected PaneBoard(double size, Game game) {
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = size;
        this.height = size;
        this.game = game;
        init();
    }

    /**
     * Initialize the PaneBoard. It create 400 cellBoard and add these in
     * listVBox and in the gridPane.
     */
    private void init() {
        this.setMaxSize(this.width + 4, this.height + 4);   // +4 for the border
        this.setMinSize(this.width + 4, this.height + 4);
        this.setStyle("-fx-border-width: 2px;"
                + "-fx-border-color: grey;");
        CellBoard cell;
        for (int j = 0; j < 20; j++) {
            for (int i = 0; i < 20; i++) {
                cell = new CellBoard(width, height, new Position(i, j));
                listVBox.add(cell);
                cell.setStyle(getStyle("black"));
                cell.setMaxSize(width / 20, height / 20);
                cell.setMinSize(width / 20, height / 20);
                setEvents(cell);
                this.add(cell, i, j);
            }
        }
    }

    /**
     * Set the background color of a CellBoard.
     *
     * @param pos The position of the CellBoard.
     * @param color The color of the CellBoard.
     */
    private void setColor(Position pos, String color) {
        CellBoard cell = getCell(pos);
        if (cell != null) {
            cell.setStyle(getStyle(color));
        }
    }

    /**
     * Put the background of all CellBoard in black.
     */
    private void reset() {
        for (CellBoard cell : listVBox) {
            cell.hideX();
            cell.setStyle(getStyle("black"));
        }
    }

    /**
     * Style for a CellBoard.
     *
     * @param color The color of the background.
     * @return A string who represents a style for a VBox.
     */
    private String getStyle(String color) {
        return "-fx-background-color: " + color + "; \n"
                + "-fx-border-width: 1px;"
                + "-fx-border-color: grey;";
    }

    /**
     * Reset the board and put the colors of the GridPane with the given game.
     * If the game is stopped, it disable the GridPane.
     */
    protected void refresh() {
        reset();
        setPiece();
        if (game.getStop()) {
            for (VBox cell : listVBox) {
                cell.setDisable(true);
            }
        } else {
            for (VBox cell : listVBox) {
                cell.setDisable(false);
            }
        }
    }

    /**
     * display the piece in the board.
     */
    private void setPiece() {
        Position pos;
        if (game.getBoard() != null) {
            for (int j = 0; j < game.getBoard().getHeight(); j++) {
                for (int i = 0; i < game.getBoard().getWidth(); i++) {
                    pos = new Position(i, j);
                    if (game.getBoard().getPieceAt(pos) != null) {
                        this.setColor(pos, game.getBoard().getPieceAt(pos).
                                getColor().toString());
                    }
                }
            }
        }
    }

    /**
     * Set the events for the given cell.
     *
     * @param cell The cell.
     */
    private void setEvents(CellBoard cell) {
        setEventMouseEntered(cell);
        setEventMouseExited(cell);
        setEventMouseClicked(cell);
    }

    /**
     * Set the event on a cell when the mouse entered on it. It show the
     * selected piece.
     *
     * @param cell The cell.
     */
    private void setEventMouseEntered(CellBoard cell) {
        cell.addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent t) -> {
            showSelectedPiece(cell.getPos());
        });
    }

    /**
     * Set the event on a cell when the mouse exit the cell. It refresh the
     * board for remove the selectedPiece.
     *
     * @param cell The cell.
     */
    private void setEventMouseExited(CellBoard cell) {
        cell.addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent t) -> {
            refresh();
        });
    }

    /**
     * Set the event on a cell when the mouse is clicked on it. It try to place
     * the selected piece if this is the primaryButton. If it's the secondary,
     * it try to rotate the selected piece.
     *
     * @param cell
     */
    private void setEventMouseClicked(CellBoard cell) {
        cell.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent t) -> {
            if (t.getButton() == MouseButton.PRIMARY) {
                game.play(game.getPieceSelected(), cell.getPos());
            } else if (t.getButton() == MouseButton.SECONDARY) {
                game.rotateSelectedPiece();
                showSelectedPiece(cell.getPos());
            }
        });
    }

    /**
     * Show the selected piece. It set the colors of the cell to show the
     * selected piece and hide or show the lblX of the cell if the piece can be
     * placed at the given pos or not.
     *
     * @param pos The position of the cell on the board.
     */
    private void showSelectedPiece(Position pos) {
        String color;
        Piece piece = game.getPieceSelected();
        if (piece != null) {
            for (CellBoard cell : getCells(piece.posInBoard(pos))) {
                if (game.getBoard().canAddPiece(piece, pos, game.getPlayers().
                        getCurrentPlayer().isFirstTurn())) {
                    color = "derive( " + game.getPlayers().getCurrentColor()
                            + ", 20% )";
                    cell.hideX();
                } else {
                    color = "derive( " + game.getPlayers().getCurrentColor()
                            + ", -60% )";
                    cell.showX();
                }
                this.setColor(cell.getPos(), color);
            }
        }
    }

    /**
     * Get a the cells who contains a position from the given list.
     *
     * @param positions The list of positions.
     * @return a list of cells.
     */
    private List<CellBoard> getCells(List<Position> positions) {
        List<CellBoard> cells = new ArrayList<>();
        CellBoard cell;
        for (Position pos : positions) {
            cell = getCell(pos);
            if (cell != null) {
                cells.add(cell);
            }
        }
        return cells;
    }

    /**
     * @param pos The position.
     * @return the cell who contains the given position. If it not found, return
     * null.
     */
    private CellBoard getCell(Position pos) {
        int i = pos.getX() + (pos.getY()) * 20;
        CellBoard cell = null;
        if (i >= 0 && i < listVBox.size() && game.getBoard().isPosInBoard(pos)) {
            cell = listVBox.get(pos.getX() + (pos.getY()) * 20);
        }
        return cell;
    }

}
