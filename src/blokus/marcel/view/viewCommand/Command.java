package blokus.marcel.view.viewCommand;

import blokus.marcel.model.Game;
import blokus.marcel.model.Piece;
import blokus.marcel.model.Position;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * CLass who represents all the commands user. Each command user are verified by
 * a regex. cmd Contains the actual command user and stop are true if the user
 * want to stop.
 */
public class Command {

    final private Pattern patPlay = Pattern.compile("^play\\p{Space}\\d{1,9}"
            + "\\p{Space}\\d{1,9}\\p{Space}\\d{1,9}$");
    final private Pattern patRotate = Pattern.compile("^rotate\\p{Space}\\d{1,2}"
            + "\\p{Space}\\d{1,2}$");
    final private Pattern patShow = Pattern.compile("^show$");
    final private Pattern patStock = Pattern.compile("^stock$");
    final private Pattern patExit = Pattern.compile("^exit$");
    final private Pattern patPass = Pattern.compile("^pass$");
    private String cmd = "";
    final private Game game;
    private boolean stop = false;
    private String[] args;
    private final Scanner scan = new Scanner(System.in);

    /**
     * Create a command.
     *
     * @param game The game we want to attach.
     */
    protected Command(Game game) {
        this.game = game;
    }

    /**
     * wait a command user and launch the command.
     */
    protected void readCmd() {
        this.cmd = scan.nextLine().toLowerCase();
        launchCmd();
    }

    /**
     * Check and launch a command user.
     */
    private void launchCmd() {
        if (patPlay.matcher(this.cmd).find()) {
            play();
        } else if (patShow.matcher(this.cmd).find()) {
            show();
        } else if (patExit.matcher(this.cmd).find()) {
            exit();
        } else if (patStock.matcher(this.cmd).find()) {
            stock();
        } else if (patPass.matcher(this.cmd).find()) {
            pass();
        } else if (patRotate.matcher(this.cmd).find()) {
            rotate();
        } else {
            System.out.println("Command invalid. You have enter: " + cmd);
        }
    }

    private void rotate() {
        args = cmd.split("\\p{Space}");
        int i = Integer.valueOf(args[1]);
        int nb = Integer.valueOf(args[2]);
        game.rotate(game.getPlayers().getCurrentPlayer().getColor(), i, nb);
    }

    private void pass() {
        game.currentPlayerPass();
    }

    /**
     * Command play. Try to add a piece in the board. The user call this like
     * this: play n i j. n is the index of a piece and i and j are the position
     * of the piece.
     */
    private void play() {
        args = cmd.split("\\p{Space}");
        Piece piece = game.getPlayers().getCurrentPlayer().getStock().
                getPieceAt(Integer.valueOf(args[1]));
        Position pos = new Position(Integer.valueOf(args[2]),
                Integer.valueOf(args[3]));
        game.play(piece, pos);
    }

    /**
     * Command show, display the board.
     */
    protected void show() {
        System.out.println(game.getBoard());
    }

    /**
     * Command stock, display the stock of the current player and his number.
     */
    protected void stock() {
        System.out.println(
                "player color: " + game.getPlayers().getCurrentPlayer());
        System.out.println(game.getPlayers().getCurrentPlayer().getStock());
    }

    /**
     * Command exit, put stop to true for stop to play.
     */
    private void exit() {
        stop = true;
    }

    /**
     * return the boolean stop, if it's value is true, stop to play.
     *
     * @return The boolean stop.
     */
    protected boolean getStop() {
        return stop;
    }
}
