package blokus.marcel.view.viewCommand;

import blokus.marcel.model.Game;
import blokus.marcel.mvc.Observer;

/**
 * Class who represent a viewCommand. It allow a user to play in the console.
 * The command are show, play, exit and stock. For more details, see class
 * Command.
 *
 * @author flo
 */
public class ViewCommand implements Observer {

    private Game game;
    private Command cmd;

    /**
     * Create a ViewCommand.
     *
     * @param game The game.
     * @throws IllegalArgumentException If game is null.
     */
    public ViewCommand(Game game) {
        if (game == null) {
            throw new IllegalArgumentException("game must be not null");
        }
        this.game = game;
        this.cmd = new Command(game);
        game.addObserver(this);
    }

    /**
     * Launch the game.
     */
    public void launch() {
        update();
        while (!game.getStop() && !cmd.getStop()) {
            cmd.readCmd();
        }
    }

    /**
     * Update the display.
     */
    @Override
    public void update() {
        cmd.stock();
        cmd.show();
        System.out.println("Turn of player "
                + game.getPlayers().getCurrentPlayer().getColor());
    }
}
