package blokus.marcel.view;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * Class who represents a TopMenu. It extends MenuBar. It contains 3 menu: file
 * and help.
 */
public class TopMenu extends MenuBar {

    private final double width;
    private final double height;
    private Menu menuHelp;
    private MenuItem menuHowPlay;
    private MenuItem menuExit;
    private Alert infoHelp;

    /**
     * Create and initialize a TopMenu.
     *
     * @param width the width of the TopMenu.
     * @param height the height of the TopMenu.
     */
    protected TopMenu(double width, double height) {
        super();
        this.width = width;
        this.height = height;
        init();
    }

    /**
     * Initialize a TopMenu. It create 3 menus.
     */
    private void init() {
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        Menu menuFile = new Menu("File");
        initMenuExit();
        menuHelp = new Menu("Help");
        menuHowPlay = new MenuItem("How to play");
        initInfoHelp();
        menuFile.getItems().add(menuExit);
        menuHelp.getItems().add(menuHowPlay);
        this.getMenus().addAll(menuFile, menuHelp);
    }

    /**
     * Initialize the menuExit.
     */
    private void initMenuExit() {
        menuExit = new MenuItem("exit");
        setEventExit();
    }

    /**
     * Set the event for the menu help. It show the infoHelp.
     */
    private void setEventHelp() {
        menuHowPlay.addEventHandler(ActionEvent.ACTION, (ActionEvent t) -> {
            infoHelp.show();
        });
    }

    /**
     * Initialize the InfoHelp.
     */
    private void initInfoHelp() {
        infoHelp = new Alert(Alert.AlertType.INFORMATION);
        infoHelp.setTitle("Help");
        infoHelp.setHeaderText("How to play");
        infoHelp.setContentText(getStrHowPlay());
        setEventHelp();
    }

    /**
     * @return A string who explain how to play.
     */
    private String getStrHowPlay() {
        return "Click on the button new game for launch a new game. \n"
                + "- Select a piece in your stock by clicking on a piece. \n"
                + "- place it on the board in a valid position. \n"
                + "- In the beginning each player have to place a cell of a "
                + "piece in a corner. \n"
                + "- The blue player begin in the bottom left corner, the yellow "
                + "in the upper left,...\n"
                + "- Each piece of a player have to connect at an other piece by "
                + "a corner. \n"
                + "- 2 pieces of the same color can't be in contact. \n"
                + "- You can rotate with the right click \n"
                + "- You can mirror a piece with the button mirror \n"
                + "- You can pass for the rest of the game with the button pass \n"
                + "- You can end the game with the button stop.\n"
                + "Have fun.";
    }

    /**
     * Set the event for the exit menu. It close the application.
     */
    private void setEventExit() {
        menuExit.addEventHandler(ActionEvent.ACTION, (ActionEvent t) -> {
            System.exit(0);
        });
    }
}
