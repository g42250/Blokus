package blokus.marcel.view;

import blokus.marcel.model.Game;
import javafx.scene.layout.VBox;

/**
 * Class who represents a Root. It extends VBox. It's The base of the window. It
 * contains all the children of the window. It contains a TopMenu, a GameHBox
 * and a ButtonHBox. The spacing between each children is 10px.
 *
 * @author flo
 */
public class Root extends VBox {

    private final int width;
    private final int height;
    private TopMenu topMenu;
    private GameHBox gameHBox;
    private ButtonHBox buttonHBox;

    /**
     * Create and initialize a Root.
     *
     * @param width The width of the root.
     * @param height The height of the root.
     * @param game The game of the application.
     */
    protected Root(int width, int height, Game game) {
        super(5);
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = width;
        this.height = height;
        init(game);
    }

    /**
     * Initialize a root. It create a TopMenu, a GameHBOx and a ButtonHBox.
     *
     * @param game The game of the application.
     */
    private void init(Game game) {
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        topMenu = new TopMenu(width, 25);
        gameHBox = new GameHBox(width, height - 100, game);
        buttonHBox = new ButtonHBox(width, 60, game);
        this.getChildren().addAll(topMenu, gameHBox, buttonHBox);
    }

    /**
     * Return the GameHBox.
     *
     * @return The GameHBox.
     */
    protected GameHBox getGameHBox() {
        return gameHBox;
    }

    /**
     * @return The buttonHBox.
     */
    public ButtonHBox getButtonHBox() {
        return buttonHBox;
    }

}
