package blokus.marcel.view;

import blokus.marcel.model.Color;
import blokus.marcel.model.Game;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Class who represents a PiecePane. It extends GridPane. It contains a list of
 * 16 VBox represented in an array of 4x4. These VBox will have a background
 * color for represented a piece.
 */
public class PiecePane extends GridPane {

    private double width;
    private double height;
    private int nbPlayer;
    private int nbPiece;
    private Game game;
    private List<VBox> listVbox;

    /**
     * Create and initialize a PiecePane.
     *
     * @param size The size of the PiecePane.
     * @param nbPlayer The number of the player.
     * @param nbPiece The index of the piece in the stock.
     * @param game The game.
     * @throws IllegalArgumentException if the game is null.
     */
    protected PiecePane(double size, int nbPlayer, int nbPiece, Game game) {
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = size - 2;
        this.height = this.width;
        this.nbPlayer = nbPlayer;
        this.nbPiece = nbPiece;
        this.game = game;

        listVbox = new ArrayList<>();
        init();
    }

    /**
     * Initialize a PiecePane. It create 16 VBox represented in an array of 4x4.
     */
    private void init() {
        VBox cell;
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < 4; i++) {
                cell = new VBox();
                cell.setMaxSize((width / 4) - 1, (height / 4) - 1);
                cell.setMinSize((width / 4) - 1, (height / 4) - 1);
                cell.setStyle(getStyle(0.75, "grey"));
                listVbox.add(cell);
                this.add(cell, i, j);
            }
        }
        setEvent();
    }

    /**
     * Set the style of a Vbox in position (x,y) with the Color color in
     * background.
     *
     * @param x The position of the VBox in x.
     * @param y The position of the VBox in y.
     * @param color The color of the background we want.
     */
    protected void setColor(int x, int y, Color color) {
        listVbox.get(x + y * 4).setStyle("-fx-background-color: " + color + ";"
                + "-fx-border-width: 0.75px;"
                + "-fx-border-color: grey;");
    }

    /**
     * reset the style of all VBox.
     */
    protected void reset() {
        for (VBox cell : listVbox) {
            cell.setStyle(getStyle(0.75, "grey"));
        }
    }

    /**
     * Create a string who represent a style with a given size and color of
     * border.
     *
     * @param sizeBorder The size of the border.
     * @param color The color of the border.
     * @return A string who represent a style.
     */
    private String getStyle(double sizeBorder, String color) {
        return "-fx-border-width: " + sizeBorder + "px;"
                + "-fx-border-color: " + color + ";";
    }

    /**
     * Set the event when the mouse is clicked on the PiecePane. It set the
     * selectedPiece on the game.
     */
    private void setEvent() {
        this.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent t) -> {
            if (game.getPlayers().get(nbPlayer).getColor()
                    == game.getPlayers().getCurrentColor()) {
                game.selectPiece(nbPlayer, nbPiece);
            }
        });
    }
}
