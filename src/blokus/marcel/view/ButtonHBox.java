package blokus.marcel.view;

import blokus.marcel.model.Game;

import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 * Class who represents a ButtonHbox. It extends HBox. This HBox have an height,
 * a width, a list of button and each button have a minimum width and height of
 * 50 and 25. The spacing between each children is 15px.
 */
public class ButtonHBox extends HBox {

    private final double height;
    private final double width;
    private final static double MINWDITHBUTTON = 50;
    private final static double MINHEIGHTBUTTON = 25;
    private Button buttonNew;
    private Button buttonPass;
    private Button buttonStop;
    private Button buttonRotate;
    private Button buttonMirror;
    private final Game game;

    /**
     * Create and initialize a ButtonHBox.
     *
     * @param width The width of the HBox.
     * @param height The height of the HBox.
     * @param game The game
     */
    protected ButtonHBox(double width, double height, Game game) {
        super(10);
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = width;
        this.height = height;
        this.game = game;
        init();
    }

    /**
     * Initialize a ButtonHbox and each button. It create 5 buttons.
     */
    private void init() {
        this.setAlignment(Pos.TOP_CENTER);
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        buttonNew = initButton("new game");
        buttonPass = initButton("pass");
        buttonStop = initButton("stop");
        buttonRotate = initButton("rotate");
        buttonMirror = initButton("mirror");
        buttonNew.setVisible(true);
        setEvents();
    }

    /**
     * Set the events for the buttons.
     */
    private void setEvents() {
        setEventBegin(buttonNew);
        setEventPass(buttonPass);
        setEventStop(buttonStop);
        setEventRotate(buttonRotate);
        setEventMirror(buttonMirror);
    }

    /**
     * Create a button.
     *
     * @param str The string of the button we want to create.
     */
    private Button initButton(String str) {
        Button button = new Button(str);
        button.setMinWidth(MINWDITHBUTTON);
        button.setMinHeight(MINHEIGHTBUTTON);
        this.getChildren().add(button);
        button.setVisible(false);
        return button;
    }

    /**
     * Set the event to the pass button. It make the player pass and he can't
     * play anymore.
     *
     * @param button The button.
     */
    private void setEventPass(Button button) {
        button.addEventHandler(ActionEvent.ACTION, (ActionEvent t) -> {
            game.currentPlayerPass();
        });
    }

    /**
     * Set the event for the begin button. It initialize and launch the game.
     *
     * @param button The button.
     */
    private void setEventBegin(Button button) {
        button.addEventHandler(ActionEvent.ACTION, (ActionEvent t) -> {
            game.beginGame();
        });
    }

    /**
     * Set the event for the stop button. It stop the game.
     *
     * @param button The button.
     */
    private void setEventStop(Button button) {
        button.addEventHandler(ActionEvent.ACTION, (ActionEvent t) -> {
            game.setStop(true);
        });
    }

    /**
     * Set the event for the button rotate. It rotate the selected piece.
     *
     * @param button The button.
     */
    private void setEventRotate(Button button) {
        button.addEventHandler(ActionEvent.ACTION, (ActionEvent t) -> {
            game.rotateSelectedPiece();
        });
    }

    /**
     * Set the event for the button mirror. It mirror the selected piece.
     *
     * @param button
     */
    private void setEventMirror(Button button) {
        button.addEventHandler(ActionEvent.ACTION, (ActionEvent t) -> {
            game.mirrorSelectedPiece();
        });
    }

    /**
     * Refresh the diplay. Only show the button new Game if stop is true. Else
     * show only the others buttons.
     */
    protected void refresh() {
        if (!game.getStop() && game.getPieceSelected() != null) {
            buttonNew.setVisible(false);
            buttonPass.setVisible(true);
            buttonStop.setVisible(true);
            buttonRotate.setVisible(true);
            buttonMirror.setVisible(true);
        } else if (!game.getStop()) {
            buttonNew.setVisible(false);
            buttonPass.setVisible(true);
            buttonStop.setVisible(true);
            buttonRotate.setVisible(false);
            buttonMirror.setVisible(false);
        } else {
            buttonNew.setVisible(true);
            buttonPass.setVisible(false);
            buttonStop.setVisible(false);
            buttonRotate.setVisible(false);
            buttonMirror.setVisible(false);
        }
    }
}
