package blokus.marcel.view;

import blokus.marcel.model.Game;
import blokus.marcel.mvc.Observer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import static javafx.application.Application.launch;

/**
 * Class who represents a Window. It display a game of Blokus with the board and
 * the stat of players.
 */
public class BlokusApplication extends Application implements Observer {

    private final Game game;
    private Scene scene;
    private final int width;
    private final int height;
    private Stage primaryStage;
    private PaneBoard paneBoard;
    private AllStatPlayerVBox allStatsPlayer;
    private ButtonHBox buttonHBox;
    private InformationWinner infoWinner;

    /**
     * Create a window of size 1000x730.
     */
    public BlokusApplication() {
        this.width = 1000;
        this.height = 730;
        this.game = new Game(20, 20);
        game.addObserver(this);
    }

    /**
     * Start the window. Add a piece for test.
     *
     * @param primaryStage The stage.
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        Root root = new Root(width, height, game);
        paneBoard = root.getGameHBox().getPaneBoard();
        allStatsPlayer = root.getGameHBox().getAllStatPlayer();
        buttonHBox = root.getButtonHBox();
        this.primaryStage.setTitle("Blokus");
        this.scene = new Scene(root, width, height);
        this.primaryStage.setScene(scene);
        infoWinner = new InformationWinner(game);
        initStage();
        this.primaryStage.show();
    }

    /**
     * Make the stage not resizable.
     */
    private void initStage() {
        this.primaryStage.setMaxHeight(height);
        this.primaryStage.setMaxWidth(width);
        this.primaryStage.setMinHeight(height);
        this.primaryStage.setMinWidth(width);
        this.primaryStage.setResizable(false);
    }

    /**
     * Open the windows.
     *
     * @param strings An array of String.
     */
    public void open(String... strings) {
        launch(strings);
    }

    /**
     * Update the display.
     */
    @Override
    public void update() {
        refresh();
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * refresh the display. It include the allStatsPlayer, the paneBoard, the
     * buttonHBox and the infoWinner.
     */
    private void refresh() {
        allStatsPlayer.refresh();
        paneBoard.refresh();
        buttonHBox.refresh();
        infoWinner.refresh();
    }

    /**
     * Launch the window.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        new BlokusApplication().open(args);
    }
}
