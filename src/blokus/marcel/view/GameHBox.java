package blokus.marcel.view;

import blokus.marcel.model.Game;
import javafx.scene.layout.HBox;

/**
 * Class who represent a GameHBox. It extends HBox. It have a width, an height,
 * a paneBoard and a allStatPlayerVBox. It have a spacing between each children
 * of 7% -3 of the width.
 */
public class GameHBox extends HBox {

    private final int width;
    private final int height;
    private PaneBoard paneBoard;
    private AllStatPlayerVBox allStatPlayer;

    /**
     * Create and initialize a GameHBox.
     *
     * @param width The width of the HBox.
     * @param height The height of the HBox.
     * @param game The game of the application
     */
    protected GameHBox(int width, int height, Game game) {
        super(67);
        if (game == null) {
            throw new IllegalArgumentException("game must not be null");
        }
        this.width = width;
        this.height = height;
        init(game);
    }

    /**
     * Initialize the GameHBox.
     *
     * @param game The game of the application.
     */
    private void init(Game game) {
        this.setMaxSize(width, height);
        this.setMinSize(width, height);
        this.paneBoard = new PaneBoard(600, game);
        allStatPlayer = new AllStatPlayerVBox(323, 630, game);
        this.getChildren().addAll(allStatPlayer, paneBoard);
    }

    /**
     * return the paneBoard.
     *
     * @return the paneBoard.
     */
    protected PaneBoard getPaneBoard() {
        return paneBoard;
    }

    /**
     * Return the allStatPlayer.
     *
     * @return the allStatPlayer.
     */
    protected AllStatPlayerVBox getAllStatPlayer() {
        return allStatPlayer;
    }
}
