package blokus.marcel;

import blokus.marcel.model.Game;
import blokus.marcel.view.viewCommand.ViewCommand;

/**
 * Class who contains the main method. Launch a game of blokus in the view
 * command.
 */
public class BlokusMarcel {

    /**
     * Main method, launch a game of blokus.
     *
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        start();
    }

    /**
     * Launch the view command.
     */
    private static void start() {
        Game game = new Game(20, 20);
        game.beginGame();
        ViewCommand vCmd = new ViewCommand(game);
        vCmd.launch();
    }
}
