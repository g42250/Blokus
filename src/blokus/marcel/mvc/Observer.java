package blokus.marcel.mvc;

/**
 * Interface who represents an observers. Each observers are attached to a
 * subject. When this object is modify, it will notify his observers and launch
 * their update method.
 */
public interface Observer {

    /**
     * Method for update the display of an observer. It will be launch by a
     * subject.
     */
    public void update();
}
