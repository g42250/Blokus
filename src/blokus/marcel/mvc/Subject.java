package blokus.marcel.mvc;

/**
 * Interface who represents a subject. Each subject have observers, when the
 * subject is modify, he will notify their observers and launch their update
 * method.
 */
public interface Subject {

    /**
     * Add an observer to the list of observer attached at the subject. This
     * observer will be now notify by any changes in the subject for reloading
     * the display.
     *
     * @param obs The observer we want to attached at the subject.
     */
    public void addObserver(Observer obs);

    /**
     * Remove an observer at the list of observer attached to the subject. It
     * will nor more be notify by the changes in the subject. If the observer is
     * not in the list, do nothing.
     *
     * @param obs The observer we want to remove.
     */
    public void removeObserver(Observer obs);
}
